kind: pipeline
type: docker
name: CI-pipeline

trigger:
  branch:
    - master
    - dev
    - feature/*
  event:
    - push
  ref:
    exclude:
    - refs/tags/no-ci

steps:
# Build Check
  - name: build
    image: mcr.microsoft.com/dotnet/sdk:7.0
    commands:
      - cd MCTG/
      - dotnet restore CI-CD.slnf
      - dotnet build CI-CD.slnf -c Release --no-restore
      - dotnet publish CI-CD.slnf -c Release --no-restore -o CI_PROJECT_DIR/build/release

# Unit Testing Check
  - name: tests
    image: mcr.microsoft.com/dotnet/sdk:7.0
    commands:
      - cd MCTG/
      - dotnet restore CI-CD.slnf
      - dotnet test CI-CD.slnf --no-restore
    depends_on: [ build ]

# Code Inspection (Sonar)
  - name: code-analysis
    image: hub.codefirst.iut.uca.fr/marc.chevaldonne/codefirst-dronesonarplugin-dotnet7
    secrets: [ SECRET_SONAR_LOGIN ]
    environment:
      sonar_host: 'https://codefirst.iut.uca.fr/sonar/'
      sonar_token:
        from_secret: SECRET_SONAR_LOGIN
      project_key: 'SAE-2.01_MCTG'
      coverage_exclusions: 'Tests/**'
    commands:
      - cd MCTG/
      - dotnet restore CI-CD.slnf
      - dotnet sonarscanner begin /k:$${project_key} /d:sonar.host.url=$${sonar_host} /d:sonar.coverageReportPaths="coveragereport/SonarQube.xml" /d:sonar.coverage.exclusions=$${coverage_exclusions} /d:sonar.login=$${sonar_token} 
      - dotnet build CI-CD.slnf -c Release --no-restore
      - dotnet test CI-CD.slnf --logger trx --no-restore /p:CollectCoverage=true /p:CoverletOutputFormat=cobertura --collect "XPlat Code Coverage"
      - reportgenerator -reports:"**/coverage.cobertura.xml" -reporttypes:SonarQube -targetdir:"coveragereport"
      - dotnet publish CI-CD.slnf -c Release --no-restore -o CI_PROJECT_DIR/build/release
      - dotnet sonarscanner end /d:sonar.login=$${sonar_token}
    depends_on: [ tests ]

# Documentation generation
  - name: generate-and-deploy-docs
    image: hub.codefirst.iut.uca.fr/alexandre.agostinho/codefirst-docdeployer:latest
    failure: ignore
    volumes:
      - name: docs
        path: /docs
    commands:
      - /entrypoint.sh -l ./Documentation/doxygen -t doxygen -d SAE-2.01_MCTG-documentation
    when:
      branch:
        - master
    depends_on: [ build ]

---

kind: pipeline
type: docker
name: CD-pipeline

trigger:
  branch:
    - master
    - dev
  event:
    - push

steps:
# Docker build and push image console-app
  - name: docker-image-console-app
    image: plugins/docker
    settings:
      dockerfile: MCTG/ConsoleApp/Dockerfile
      context: MCTG/
      registry: hub.codefirst.iut.uca.fr
      repo: hub.codefirst.iut.uca.fr/alexandre.agostinho/console-mctg
      username:
        from_secret: SECRET_REGISTRY_USERNAME
      password:
        from_secret: SECRET_REGISTRY_PASSWORD
