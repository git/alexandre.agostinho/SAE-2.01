﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    [DataContract(Name = "passmgr")]
    public class PasswordSHA256Manager : IPasswordManager
    {
        public string HashPassword(string password)
        {
            byte[] data;
            using (SHA256 sha256Hash = SHA256.Create())
                data = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(password));

            var sb = new StringBuilder();
            foreach (byte b in data) sb.Append(b.ToString("x2"));

            return sb.ToString();
        }

        public bool VerifyPassword(string hashedPassword, string passwordEntered)
        {
            string hashedInput = HashPassword(passwordEntered);
            StringComparer strcmp = StringComparer.OrdinalIgnoreCase;

            return strcmp.Compare(hashedPassword, hashedInput) == 0;
        }
    }
}
