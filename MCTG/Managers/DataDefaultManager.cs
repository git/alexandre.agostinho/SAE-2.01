﻿using Model;

namespace Managers
{
    /// <summary>
    /// Define the manager of the data. This is where all the data are put, and where we call the loading and the saving of them.
    /// </summary>
    public class DataDefaultManager : IDataManager
    {
        #region Attributes & Properties
        public IDataSerializer Serializer { get; set; }

        public IDictionary<string, List<object>> Data { get; private set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor of the DataDefaultManager class. Take a IDataManager that will provide methods for the serialisation of the data.
        /// </summary>
        /// <param name="dataSerializer">The data manager that know how to serialize a file.</param>
        /// <param name="data">The data set of the application.</param>
        public DataDefaultManager(IDataSerializer dataSerializer,
                                  IDictionary<string, List<object>>? data = null)
        {
            Serializer = dataSerializer;

            if (data is null)
                Data = new Dictionary<string, List<object>>();
            else
                Data = data;
        }
        #endregion

        #region Methods
        public void LoadData()
            => Data = Serializer.Load();

        public void SaveData()
            => Serializer.Save(Data);

        public void Import<T>(string pathOfTheFile)
            where T : class
        {
            KeyValuePair<string, T> import = Serializer.Import<T>(pathOfTheFile);
            Data[import.Key].Add(import.Value);
        }

        public void Export<T>(T obj, string pathToExport)
            where T : class
            => Serializer.Export<T>(obj, pathToExport);

        public ICollection<T> GetFromData<T>() where T : class
            => new List<T>(Data[typeof(T).Name].Cast<T>());
        #endregion
    }
}
