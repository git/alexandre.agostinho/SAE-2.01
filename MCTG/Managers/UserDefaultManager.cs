﻿using AppException;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Managers
{
    public class UserDefaultManager : IUserManager, INotifyPropertyChanged
    {
        private IDataManager _dataManager;
        private IPasswordManager _passwordManager;
        private User? _currentConnected = null;


        public UserDefaultManager(IDataManager dataManager, IPasswordManager passwordManager)
        {
            _dataManager = dataManager;
            _passwordManager = passwordManager;
        }


        public User? CurrentConnected
        {
            get => _currentConnected;
            private set
            {
                _currentConnected = value;
                OnPropertyChanged();
            }
        }

        public IPasswordManager PasswordManager => _passwordManager;

        public event PropertyChangedEventHandler? PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string pname = "")
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(pname));


        public bool AddUserToData(User user)
        {
            var userList = _dataManager.Data[nameof(User)];

            if (userList.Exists(r => r.Equals(user)))
                return false;

            _dataManager.Data[nameof(User)].Add(user);
            return true;
        }

        public User CreateUser(string mail, string password,
                               string? name = null, string? surname = null, string? profilePict = null)
        {
            if (name is null) name = $"user{GetRandomInt()}";
            if (surname is null) surname = "";
            if (profilePict is null) profilePict = "default_user_picture.png";

            const string mailRegex = @"^([\w\.-]+)@([\w\.-]+\.\w+)$";
            if (!Regex.Match(mail, mailRegex).Success)
                throw new BadMailFormatException();

            string hashedPassword = PasswordManager.HashPassword(password);
            return new User(name, surname, mail, hashedPassword, profilePict);
        }

        public ICollection<User> GetAllUsers()
        {
            return _dataManager.GetFromData<User>();
        }

        public User GetUserFromMail(string mail)
        {
            User? user = _dataManager.GetFromData<User>().ToList()
                                                         .Find(u => u.Mail == mail);
            if (user is null)
                throw new UserNotFoundException();

            return user;
        }

        public bool LogIn(string mail, string password)
        {
            if (CurrentConnected is not null)
                throw new UserAlreadyConnectedException();

#if DEBUG
            if (mail == "admin")
            {
                CurrentConnected = _dataManager.GetFromData<User>()
                                                    .FirstOrDefault(u => u.Mail == "admin@mctg.fr");
                return true;
            }
#endif

            User? user = _dataManager.GetFromData<User>().ToList()
                                                         .Find(u => u.Mail == mail);
            if (user is null)
                return false;

            if (!_passwordManager.VerifyPassword(user.Password, password))
                return false;

            CurrentConnected = user;
            return true;
        }

        public void LogOut()
        {
            if (CurrentConnected is null)
                throw new NoUserConnectedException();

            CurrentConnected = null;
        }

        public bool ModifyCurrentConnected(User newUser)
        {
            try
            {
                var index = _dataManager.GetFromData<User>().ToList()
                                                            .FindIndex(u => u.Equals(CurrentConnected));
                _dataManager.Data[nameof(User)][index] = newUser;
            }
            catch (ArgumentNullException e)
            {
                Debug.WriteLine("User to modify not found.\n" + e.Message);
                return false;
            }

            return true;
        }

        private int GetRandomInt()
        {
            var randomGenerator = RandomNumberGenerator.Create();
            byte[] data = new byte[16];
            randomGenerator.GetBytes(data);
            return Math.Abs(BitConverter.ToInt16(data));
        }
    }
}
