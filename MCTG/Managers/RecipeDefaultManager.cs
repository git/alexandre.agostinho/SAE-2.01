﻿using AppException;
using Model;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Managers
{
    public class RecipeDefaultManager : IRecipeManager
    {
        private IDataManager _dataManager;

        public Recipe? CurrentSelected { get; set; } = null;


        public RecipeDefaultManager(IDataManager dataManager)
        {
            _dataManager = dataManager;
        }


        public bool AddRecipeToData(Recipe recipe)
        {
            var recipeList = _dataManager.Data[nameof(Recipe)];

            if (recipeList.Exists(r => r.Equals(recipe)))
                return false;

            _dataManager.Data[nameof(Recipe)].Add(recipe);
            return true;
        }

        public RecipeCollection GetAllRecipes()
        {
            return new RecipeCollection(
                "All recipes",
                _dataManager.GetFromData<Recipe>().ToArray());
        }

        public RecipeCollection GetRecipeByAuthor(string authorMail)
        {
            User? author = _dataManager.GetFromData<User>()
                                       .ToList()
                                       .Find(u => u.Mail == authorMail);
            if (author is null)
                throw new UserNotFoundException(authorMail);

            IEnumerable<Recipe> recipes = from Recipe r in _dataManager.GetFromData<Recipe>()
                                          where r.AuthorMail == author.Mail
                                          select r;

            return new RecipeCollection(
                $"{author.Name} {author.Surname}'s recipes", recipes.ToArray());
        }

        public RecipeCollection SearchRecipeByTitle(string title)
        {
            IEnumerable<Recipe> recipes = from Recipe recipe in _dataManager.GetFromData<Recipe>()
                                          where recipe.Title.ToLower().Contains(title.ToLower())
                                          select recipe;
            return new RecipeCollection(
                $"Search for '{title}'", recipes.ToArray());
        }

        public Recipe GetRecipeFromId(int id)
        {
            Recipe? recipe = _dataManager.GetFromData<Recipe>()
                                        .ToList()
                                        .Find(r => r.Id == id);
            if (recipe is null)
                throw new RecipeNotFoundException();

            return recipe;
        }

        public RecipeCollection GetRecipesByPriorityOrder(IEnumerable<Priority> priorities)
        {
            List<Recipe> recipes = new List<Recipe>();
            IEnumerable<Recipe> recipesWithCurrentPriority;
            foreach (Priority priority in priorities)
            {
                recipesWithCurrentPriority = from Recipe recipe in _dataManager.GetFromData<Recipe>()
                                             where recipe.Priority == priority
                                             select recipe;
                recipes.AddRange(recipesWithCurrentPriority);
            }

            return new RecipeCollection(
                $"Suggestions", recipes.ToArray());
        }

        public bool ModifyCurrentSelected(Recipe newRecipe)
        {
            if (CurrentSelected is null)
                throw new NoRecipeSelectedException();

            try
            {
                var index = _dataManager.GetFromData<Recipe>().ToList()
                                                            .FindIndex(u => u.Equals(CurrentSelected));
                _dataManager.Data[nameof(Recipe)][index] = newRecipe;
            }
            catch (ArgumentNullException e)
            {
                Debug.WriteLine("Recipe to modify not found.");
                return false;
            }

            return true;
        }
    }
}
