﻿using Model;
using System.Runtime.Serialization;
using System.Xml;

namespace DataPersistence
{
    /// <summary>
    /// Define a serializer to manage XML files.
    /// </summary>
    public class DataContractXML : IDataSerializer
    {
        #region Attributes
        private string _xmlFolderPath;
        private XmlWriterSettings _xmlWriterSettings;
        private DataContractSerializerSettings _dataContractSerializerSettings;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor of a DataContractXML serializer.
        /// </summary>
        /// <param name="xmlFolderPath">Give the default path where to load and save the data (by default is the current execution dir).</param>
        /// <param name="xmlWriterSettings">Give another set of XML setting to write file.</param>
        /// <param name="dataContractSerializerSettings">Give another set of DataContract serializer setting to write file</param>
        public DataContractXML(string xmlFolderPath = "",
                               XmlWriterSettings? xmlWriterSettings = null,
                               DataContractSerializerSettings? dataContractSerializerSettings = null)
        {
            _xmlFolderPath = xmlFolderPath;

            if (xmlWriterSettings is null)
                _xmlWriterSettings = new XmlWriterSettings() { Indent = true };
            else
                _xmlWriterSettings = xmlWriterSettings;

            if (dataContractSerializerSettings is null)
                _dataContractSerializerSettings = new DataContractSerializerSettings()
                {
                    KnownTypes = new Type[]
                    {
                        typeof(Recipe),
                        typeof(RecipeType),
                        typeof(Priority),
                        typeof(Review),
                        typeof(User),
                        typeof(Ingredient),
                        typeof(Quantity)
                    },
                    PreserveObjectReferences = true
                };
            else
                _dataContractSerializerSettings = dataContractSerializerSettings;
        }
        #endregion

        #region IDataManager implementation
        public void Export<T>(T obj, string pathToExport)
            where T : class
        {
            bool restore = _dataContractSerializerSettings.PreserveObjectReferences;
            _dataContractSerializerSettings.PreserveObjectReferences = false;
            var serializer = new DataContractSerializer(typeof(T), _dataContractSerializerSettings);
            using (TextWriter textWriter = File.CreateText(Path.Combine(_xmlFolderPath, pathToExport)))
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, _xmlWriterSettings))
                {
                    serializer.WriteObject(xmlWriter, obj);
                }
            }
            _dataContractSerializerSettings.PreserveObjectReferences = restore;
        }

        public KeyValuePair<string, T> Import<T>(string pathToImport)
            where T : class
        {
            T? obj;
            var serializer = new DataContractSerializer(typeof(T), _dataContractSerializerSettings);
            using (Stream s = File.OpenRead(Path.Combine(_xmlFolderPath, pathToImport)))
            {
                obj = serializer.ReadObject(s) as T;
            }

            if (obj is null)
                throw new ArgumentNullException("obj");

            string typeName = typeof(T).Name;
            return new KeyValuePair<string, T>(typeName, obj);
        }

        public IDictionary<string, List<object>> Load()
        {
            Dictionary<string, List<object>>? elements;
            var serializer = new DataContractSerializer(typeof(Dictionary<string, List<object>>), _dataContractSerializerSettings);
            using (Stream s = File.OpenRead(Path.Combine(_xmlFolderPath, "data.xml")))
            {
                elements = serializer.ReadObject(s) as Dictionary<string, List<object>>;
            }

            if (elements is null)
                throw new ArgumentNullException("elements");

            return elements;
        }

        public void Save(IDictionary<string, List<object>> elements)
        {
            var serializer = new DataContractSerializer(typeof(Dictionary<string, List<object>>), _dataContractSerializerSettings);
            using (TextWriter textWriter = File.CreateText(Path.Combine(_xmlFolderPath, "data.xml")))
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, _xmlWriterSettings))
                {
                    serializer.WriteObject(xmlWriter, elements);
                }
            }
        }
        #endregion
    }
}
