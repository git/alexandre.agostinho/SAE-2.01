﻿using Model;
using System.Runtime.Serialization.Json;

namespace DataPersistence
{
    /// <summary>
    /// Define a serializer to manage JSON files.
    /// </summary>
    public class DataContractJSON : IDataSerializer
    {
        #region Attributes
        private string _jsonFolderPath;
        private DataContractJsonSerializerSettings _dataContractJsonSerializerSettings;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor of the DataContractJSON serializer.
        /// </summary>
        /// <param name="jsonFolderPath">Give the default path where to load and save the data (by default is the current execution dir).</param>
        /// <param name="dataContractJsonSerializerSettings">Give another set of DataContractJson serializer setting to write file</param>
        public DataContractJSON(string jsonFolderPath = "",
                                DataContractJsonSerializerSettings? dataContractJsonSerializerSettings = null)
        {
            _jsonFolderPath = jsonFolderPath;
            if (dataContractJsonSerializerSettings is null)
                _dataContractJsonSerializerSettings = new DataContractJsonSerializerSettings()
                {
                    KnownTypes = new[]
                    {
                        typeof(Recipe),
                        typeof(RecipeType),
                        typeof(Priority),
                        typeof(Review),
                        typeof(User),
                        typeof(Ingredient),
                        typeof(Quantity)
                    }
                };
            else
                _dataContractJsonSerializerSettings = dataContractJsonSerializerSettings;
        }
        #endregion

        #region IDataManager implementation
        public void Export<T>(T obj, string pathToExport)
            where T : class
        {
            var jsonSerializer = new DataContractJsonSerializer(typeof(T), _dataContractJsonSerializerSettings);
            using (FileStream stream = File.Create(Path.Combine(_jsonFolderPath, pathToExport)))
            {
                using (var jsonWriter = JsonReaderWriterFactory.CreateJsonWriter(
                    stream: stream,
                    encoding: System.Text.Encoding.UTF8,
                    ownsStream: false,
                    indent: true))
                {
                    jsonSerializer.WriteObject(jsonWriter, obj);
                }
            }
        }

        public KeyValuePair<string, T> Import<T>(string pathToImport)
            where T : class
        {
            T? obj;
            var jsonSerializer = new DataContractJsonSerializer(typeof(T), _dataContractJsonSerializerSettings);
            using (FileStream stream = File.OpenRead(Path.Combine(_jsonFolderPath, pathToImport)))
            {
                obj = jsonSerializer.ReadObject(stream) as T;
            }

            if (obj is null)
                throw new ArgumentNullException("obj");

            string typeName = typeof(T).Name;
            return new KeyValuePair<string, T>(typeName, obj);
        }

        public IDictionary<string, List<object>> Load()
        {
            Dictionary<string, List<object>>? elements = new Dictionary<string, List<object>>();
            var jsonSerializer = new DataContractJsonSerializer(typeof(Dictionary<string, List<object>>), _dataContractJsonSerializerSettings);
            using (FileStream stream = File.OpenRead(Path.Combine(_jsonFolderPath, "data.json")))
            {
                elements = jsonSerializer.ReadObject(stream) as Dictionary<string, List<object>>;
            }

            if (elements is null)
                throw new ArgumentNullException("elements");

            return elements;
        }

        public void Save(IDictionary<string, List<object>> elements)
        {
            var jsonSerializer = new DataContractJsonSerializer(typeof(Dictionary<string, List<object>>), _dataContractJsonSerializerSettings);
            using (FileStream stream = File.Create(Path.Combine(_jsonFolderPath, "data.json")))
            {
                using (var jsonWriter = JsonReaderWriterFactory.CreateJsonWriter(
                    stream: stream,
                    encoding: System.Text.Encoding.UTF8,
                    ownsStream: false,
                    indent: true))
                {
                    jsonSerializer.WriteObject(jsonWriter, elements);
                }
            }
        }
        #endregion
    }
}
