﻿using Managers;
using Model;
using System;
using System.Collections.Generic;


namespace FakePersistance
{
    /// <summary>
    /// The subs class is a group of prefabricated object that can only be loaded. It only use is for testing.
    /// </summary>
    public class Stubs : IDataSerializer
    {
        private IPasswordManager psswdMgr = new PasswordSHA256Manager();

        public IDictionary<string, List<object>> Load()
        {
            Dictionary<string, List<object>> data = new Dictionary<string, List<object>>
            {
                {
                    #region Data: Recipes
                    nameof(Recipe),
                    new List<object>(new[]
                    {
                        new Recipe("Cookies classiques", RecipeType.Dessert, Priority.Easy, null, "admin@mctg.fr", "")
                        {
                            Ingredients = new List<Ingredient>
                            {
                                new Ingredient("Patates", new Quantity(23, Unit.unit)),
                                new Ingredient("Farine", new Quantity(23, Unit.G))
                            },
                            PreparationSteps = new List<PreparationStep>
                            {
                                new PreparationStep(1, "Faire cuire."),
                                new PreparationStep(2, "Manger.")
                            },
                            Reviews = new List<Review>
                            {
                                new Review(4, "Bonne recette, je recommande !"),
                                new Review(3, "Bof bof, mais mangeable...")
                            }
                        },
                        new Recipe(
                            title: "Cookies au chocolat",
                            type: RecipeType.Dessert,
                            priority: Priority.Fast)
                        {
                            Ingredients = new List<Ingredient>
                            {
                                new Ingredient("Farine", new Quantity(200, Unit.G))
                            },
                            PreparationSteps = new List<PreparationStep>()
                            {
                                new PreparationStep(1, "Moulinez la pâte."),
                                new PreparationStep(2, "Faire cuire pendant une bonne heure."),
                                new PreparationStep(3, "Sortir du four et mettre dans un plat.")
                            }
                        },
                        new Recipe(
                            title: "Gateau nature",
                            type: RecipeType.Dessert,
                            priority: Priority.Gourmet)
                        {
                            Ingredients = new List<Ingredient>
                            {
                                new Ingredient("Farine", new Quantity(200, Unit.G)),
                                new Ingredient("Lait", new Quantity(2, Unit.L))
                            },
                            PreparationSteps = new List<PreparationStep>
                            {
                                new PreparationStep(1, "Achetez les ingrédients."),
                                new PreparationStep(2, "Préparez le matériel. Ustensiles et tout."),
                                new PreparationStep(3, "Pleurez.")
                            },
                            Reviews = new List<Review>
                            {
                                new Review("pedrosamigos@hotmail.com", 5, "C'était vraiment IN-CROY-ABLE !!!")
                            }
                        },
                        new Recipe(
                            title: "Gateau au pommes",
                            type: RecipeType.Dessert,
                            priority: Priority.Light)
                        {
                            PreparationSteps = new List<PreparationStep>
                            {
                                new PreparationStep(1, "Achetez les légumes."),
                                new PreparationStep(2, "Préparez le plat. Ustensiles et préchauffez le four."),
                                new PreparationStep(3, "Coupez les pommes en morceaux et disposez-les sur le plat."),
                                new PreparationStep(4, "Mettez enfin le plat au four, puis une fois cuit, dégustez !")
                            }
                        },
                        new Recipe(
                            title: "Gateau au chocolat",
                            type: RecipeType.Dessert,
                            priority: Priority.Economic,
                            id: null, authorMail: "pedrosamigos@hotmail.com")
                        {
                            Ingredients = new List<Ingredient>
                            {
                                new Ingredient("Mais", new Quantity(2, Unit.kG)),
                                new Ingredient("Sachet pépites de chocolat", new Quantity(1, Unit.unit)),
                                new Ingredient("Dinde", new Quantity(2, Unit.G))
                            },
                            PreparationSteps = new List<PreparationStep>
                            {
                                new PreparationStep(1, "Ajouter les oeufs."),
                                new PreparationStep(2, "Ajouter la farine."),
                                new PreparationStep(3, "Ajouter 100g de chocolat fondu."),
                                new PreparationStep(4, "Mélanger le tout."),
                                new PreparationStep(5, "Faire cuire 45h au four traditionnel.")
                            }
                        },
                        new Recipe(
                            title: "Dinde au jambon",
                            type: RecipeType.Dish,
                            priority: Priority.Easy,
                            id: null, authorMail: "pedrosamigos@hotmail.com")
                        {
                            Ingredients = new List<Ingredient>
                            {
                                new Ingredient("Morceaux de bois", new Quantity(2, Unit.unit)),
                                new Ingredient("Sachet gélatine", new Quantity(1, Unit.unit)),
                                new Ingredient("Jambon", new Quantity(2, Unit.kG))
                            },
                            PreparationSteps = new List<PreparationStep>
                            {
                                new PreparationStep(1, "Faire une cuisson bien sec de la dinde à la poêle"),
                                new PreparationStep(2, "Mettre la dinde au frigo."),
                                new PreparationStep(3, "Mettre le jambon dans le micro-onde."),
                                new PreparationStep(4, "Faire chauffer 3min."),
                                new PreparationStep(5, "Présentez sur un plat la dinde et le jambon : Miam !")
                            }
                        },
                        new Recipe(
                            title: "Poulet au curry",
                            type: RecipeType.Dish,
                            priority: Priority.Gourmet,
                            id: null, authorMail: "pedrosamigos@hotmail.com")
                        {
                            Ingredients = new List<Ingredient>
                            {
                                new Ingredient("Pissenlis", new Quantity(200, Unit.unit)),
                                new Ingredient("Boule de pétanque", new Quantity(10, Unit.unit)),
                                new Ingredient("Poivre", new Quantity(4, Unit.mG))
                            },
                            PreparationSteps = new List<PreparationStep>
                            {
                                new PreparationStep(1, "Trouvez des épices de curry."),
                                new PreparationStep(2, "Trouvez maintenant du poulet."),
                                new PreparationStep(3, "Coupez la tête du poulet et posez-la dans un plat."),
                                new PreparationStep(4, "Parsemez d'épices curry la tête de la poule."),
                                new PreparationStep(5, "Mettre le tout au four traditionnel 30min."),
                                new PreparationStep(6, "Dégustez en famille !")
                            },
                            Reviews = new List<Review>
                            {
                                new Review(5, "Meilleure recette que j'ai avalé de tout les temps !!!!!!!")
                            }
                        }
                    })
                    #endregion
                },

                {
                    #region Data: User
                    nameof(User),
                    new List<object>(new[]
                    {
                        new User(
                            name: "Admin",
                            surname: "Admin",
                            mail: "admin@mctg.fr",
                            hashedPassword: psswdMgr.HashPassword("admin")),
                        new User(
                            name: "Pedros",
                            surname: "Amigos",
                            mail: "pedrosamigos@hotmail.com",
                            hashedPassword: psswdMgr.HashPassword("pamigos"))
                    })
                    #endregion
                }
            };

            return data;
        }

        #region Not supported methods
        public void Save(IDictionary<string, List<object>> elements)
        {
            throw new NotSupportedException();
        }

        public void Export<T>(T obj, string pathToExport) where T : class
        {
            throw new NotSupportedException();
        }

        public KeyValuePair<string, T> Import<T>(string pathToImport) where T : class
        {
            throw new NotSupportedException();
        }
        #endregion
    }
}
