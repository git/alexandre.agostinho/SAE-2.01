using Model;

namespace Model_UnitTests
{
    public class Recipe_UT
    {
        [Fact]
        public void TestVoidConstructor()
        {
            Recipe r = new Recipe(
                title: "test recipe", type: RecipeType.Unspecified, priority: Priority.Easy);

            Assert.NotNull(r.Title);
        }

        [Theory]
        [InlineData("recipe", RecipeType.Dish, Priority.Light, "recipe", RecipeType.Dish, Priority.Light)]
        [InlineData("No title.", RecipeType.Unspecified, Priority.Light, "", RecipeType.Unspecified, Priority.Light)]
        [InlineData("re cipe", RecipeType.Unspecified, Priority.Light, "re cipe", RecipeType.Unspecified, Priority.Light)]
        public void TestValuesConstructor(
            string expectedTitle, RecipeType expectedType, Priority expectedPriority,
            string title,         RecipeType type,         Priority priority)
        {
            Recipe rc = new Recipe(title, type, priority);

            Assert.Equal(expectedTitle, rc.Title);
            Assert.Equal(expectedType, rc.Type);
            Assert.Equal(expectedPriority, rc.Priority);
        }
    }
}
