﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace Model_UnitTests
{
    public class Ingredient_UT
    {
        public void TestConstructIngredient()
        {
            Quantity quantity = new Quantity(200, Unit.mL);
            Ingredient patate = new Ingredient("Patate", quantity);
            Assert.Equal("Patate", patate.Name);
        }
    }
}
