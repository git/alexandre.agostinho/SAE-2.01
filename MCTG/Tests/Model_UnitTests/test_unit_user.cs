﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model_UnitTests
{
    public class test_unit_user
    {
        [Fact]
        public void TestConstructUser()
        {
            PasswordSHA256Manager passwordManager = new PasswordSHA256Manager();
            User user = new User("Bob", "Dylan", "bd@gmail.com", passwordManager.HashPassword("bobby"));
            Assert.Equal("Bob", user.Name);
            Assert.Equal("Dylan", user.Surname);
            Assert.Equal("bd@gmail.com", user.Mail);
            Assert.Equal(passwordManager.HashPassword("bobby"), user.Password);
            Assert.NotNull(user.Priorities);
        }
    }
}
