using CommunityToolkit.Maui.Behaviors;
using DataPersistence;
using Model;

using System.Collections.ObjectModel;
using System.Diagnostics;

namespace Views;

public partial class MyProfil : ContentPage
{
    public MasterManager Master => (App.Current as App).Master;
    public User CurrentUser => Master.User.CurrentConnected;



    public MyProfil()
    {
        InitializeComponent();

        BindingContext = this;
    }
    public ObservableCollection<Priority> PriorityList { get; private set; } = new ObservableCollection<Priority>()
    {  Priority.Economic, Priority.Fast, Priority.Easy, Priority.Light, Priority.Gourmet };
    void DragGestureRecognizer_DragStarting2(System.Object sender, Microsoft.Maui.Controls.DragStartingEventArgs e)
    {
        e.Data.Properties["value"] = (sender as Element).Parent.BindingContext;
    }

    void DropGestureRecognizer_Drop2(System.Object sender, Microsoft.Maui.Controls.DropEventArgs e)
    {
        var receivingElement = (Priority)((sender as Element).Parent.BindingContext);

        var draggedElement = (Priority)e.Data.Properties["value"];
        int draggedIndex = PriorityList.IndexOf(draggedElement);
        PriorityList.RemoveAt(draggedIndex);

        int receivingIndex = PriorityList.IndexOf(receivingElement);
        PriorityList.Insert(receivingIndex + 1, draggedElement);
    }

    private void OnMyRecipeClicked(object sender, EventArgs e)
    {
        Navigation.PushModalAsync(new MyPosts());
    }

    private void OnAddRecipeClicked(object sender, EventArgs e)
    {
        Navigation.PushModalAsync(new AddRecipe());
    }
}