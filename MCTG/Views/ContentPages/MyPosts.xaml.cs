using CommunityToolkit.Maui.Storage;
using Model;

namespace Views;

public partial class MyPosts : ContentPage
{
    public MasterManager Master => (Application.Current as App).Master;

    private readonly RecipeCollection _recipesDisplayed;
    public ReadOnlyObservableRecipeCollection RecipesDisplayed { get; private set; }

    public Recipe Recipe => Master.Recipe.CurrentSelected;


    public MyPosts()
	{
        _recipesDisplayed = Master.Recipe.GetRecipeByAuthor(Master.User.CurrentConnected.Mail);
        RecipesDisplayed = new ReadOnlyObservableRecipeCollection(_recipesDisplayed);

        InitializeComponent();

        BindingContext = this;
    }

    private void MyInformations_Clicked(object sender, EventArgs e)
    {
        Navigation.PopModalAsync();
    }

    private void AddRecipe_Clicked(object sender, EventArgs e)
    {
        Navigation.PushModalAsync(new AddRecipe());
    }

    private async void ExportRecipe_Clicked(object sender, EventArgs e)
    {
        var result = await FolderPicker.Default.PickAsync(FileSystem.Current.AppDataDirectory, new CancellationToken());
        Master.Recipe.CurrentSelected = (Recipe)(sender as MenuFlyoutItem).BindingContext;

        if (result.IsSuccessful)
        {
            Master.Data.Export(Recipe, Path.Combine(result.Folder.Path, $"{Recipe.Id}.{Recipe.Title.Replace(" ", "")}.json"));
            await DisplayAlert("Export", "Recipe was successfully exported.", "Ok");
        }
        else
        {
            await DisplayAlert("Export", "Unable to export recipe.", "Ok");
        }
    }
}
