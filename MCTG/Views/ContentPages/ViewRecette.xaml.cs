using Model;

namespace Views;
/// <summary>
/// Classe de la page contenant le detail de la recette
///	
/// </summary>
public partial class ViewRecette : ContentPage
{
    public MasterManager Master => (Application.Current as App).Master;
    public User user => Master.User.CurrentConnected;
    public Recipe Recipe => Master.Recipe.CurrentSelected;
    public RecipeCollection AllRecipes => Master.Recipe.GetAllRecipes();
    public ViewRecette()
	{
        InitializeComponent();
        BindingContext = this;
    }


}
