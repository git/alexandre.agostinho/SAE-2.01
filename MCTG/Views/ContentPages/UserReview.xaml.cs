namespace Views;

public partial class UserReview : ContentView
{
	public UserReview()
	{
		InitializeComponent();
	}

    public static readonly BindableProperty CommentProperty =
        BindableProperty.Create("Comment", typeof(string), typeof(Label), "");

    public string Comment
    {
        get => (string)GetValue(CommentProperty);
        set => SetValue(CommentProperty, value);
    }
}
