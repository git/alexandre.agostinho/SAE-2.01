﻿using AppException;
using CommunityToolkit.Maui.Storage;
using Microsoft.Maui.Controls;
using Model;
using System.Collections.ObjectModel;

namespace Views
{
    public partial class Home : ContentPage
    {
        public MasterManager Master => (Application.Current as App).Master;

        private readonly RecipeCollection _recipesDisplayed;
        public ReadOnlyObservableRecipeCollection RecipesDisplayed { get; private set; }

        public Recipe Recipe => Master.Recipe.CurrentSelected;


        public Home()
        {
            // Master.User.LogIn("pedrosamigos@hotmail.com", "pamigos");
            if (Master.User.CurrentConnected is null)
            {
                _recipesDisplayed = Master.Recipe.GetAllRecipes();
                _recipesDisplayed.Description = "Toutes les recettes";
            }
            else
            {
                _recipesDisplayed = Master.Recipe.GetRecipesByPriorityOrder(Master.User.CurrentConnected.Priorities);
                _recipesDisplayed.Description = "Suggestions";
            }

            RecipesDisplayed = new ReadOnlyObservableRecipeCollection(_recipesDisplayed);

            InitializeComponent();

            BindingContext = this;
        }


        private void ModifyRecipesDisplayed(RecipeCollection recipes)
        {
            _recipesDisplayed.Clear();
            _recipesDisplayed.Description = recipes.Description;
            foreach (Recipe recipe in recipes)
            {
                _recipesDisplayed.Add(recipe);
            }
        }

        private void SearchBar_SearchButtonPressed(object sender, EventArgs e)
        {
            string searchStr = (sender as SearchBar).Text;
            ModifyRecipesDisplayed(Master.Recipe.SearchRecipeByTitle(searchStr));
        }

        public void OnImageClicked(object sender, EventArgs e)
        {
            Master.Recipe.CurrentSelected = (Recipe)(sender as ImageButton).BindingContext;
            Navigation.PushModalAsync(new ViewRecette());
        }

        private void Entrees_Clicked(object sender, EventArgs e)
        {
            ModifyRecipesDisplayed(new RecipeCollection("Entrées",
                                                        Master.Recipe.GetAllRecipes()
                                                        .ToList()
                                                        .FindAll(r => r.Type == RecipeType.Starter)
                                                        .ToArray()));
        }

        private void Plats_Clicked(object sender, EventArgs e)
        {
            ModifyRecipesDisplayed(new RecipeCollection("Plats",
                                                        Master.Recipe.GetAllRecipes()
                                                        .ToList()
                                                        .FindAll(r => r.Type == RecipeType.Dish)
                                                        .ToArray()));
        }

        private void Desserts_Clicked(object sender, EventArgs e)
        {
            ModifyRecipesDisplayed(new RecipeCollection("Desserts",
                                                        Master.Recipe.GetAllRecipes()
                                                        .ToList()
                                                        .FindAll(r => r.Type == RecipeType.Dessert)
                                                        .ToArray()));
        }

        private void AllRecipes_Clicked(object sender, EventArgs e)
        {
            ModifyRecipesDisplayed(new RecipeCollection("Toutes les recettes", Master.Recipe.GetAllRecipes()));
        }

        private void Suggestions_Clicked(object sender, EventArgs e)
        {
            if (Master.User.CurrentConnected is null)
                throw new NoUserConnectedException();

            ModifyRecipesDisplayed(new RecipeCollection(
                description: "Suggestions",
                recipes: Master.Recipe.GetRecipesByPriorityOrder(Master.User.CurrentConnected.Priorities)));
        }

        private async void ExportRecipe_Clicked(object sender, EventArgs e)
        {
            var result = await FolderPicker.Default.PickAsync(FileSystem.Current.AppDataDirectory, new CancellationToken());
            Master.Recipe.CurrentSelected = (Recipe)(sender as MenuFlyoutItem).BindingContext;

            if (result.IsSuccessful)
            {
                Master.Data.Export(Recipe, Path.Combine(result.Folder.Path, $"{Recipe.Id}.{Recipe.Title.Replace(" ", "")}.json"));
                await DisplayAlert("Export", "Recipe was successfully exported.", "Ok");
            }
            else
            {
                await DisplayAlert("Export", "Unable to export recipe.", "Ok");
            }
        }
    }
}
