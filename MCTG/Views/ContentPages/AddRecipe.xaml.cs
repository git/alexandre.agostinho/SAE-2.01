using Model;
using System.Diagnostics;
using Microsoft.Maui.Media;
using Microsoft.Maui.Controls;

namespace Views
{
    public partial class AddRecipe : ContentPage
    {
        private List<Ingredient> ingredientList;
        private List<PreparationStep> preparationStepList;
        private Recipe recipeToAdd;
        private Ingredient ingredient;
        private PreparationStep preparationStep;
        private string titleRecipe;

        public FileResult ImageSource { get; private set; } = null;
        public string? ImageSourcePath { get; private set; } = null;
        public MasterManager Master => (Application.Current as App).Master;
        public User CurrentUser => Master.User.CurrentConnected;
        public Recipe RecipeToAdd{ get=> recipeToAdd; set => recipeToAdd = value; }
        public List<Unit> UnitList { get; set; } = new List<Unit> { Unit.unit, Unit.kG, Unit.mG, Unit.G, Unit.L, Unit.cL, Unit.mL };
        public List<Ingredient> IngredientList { get => ingredientList; set => ingredientList = value; }
        public List<PreparationStep> PreparationStepList { get => preparationStepList; set => preparationStepList = value; }
        public Ingredient Ingredient { get => ingredient; set => ingredient = value ; }
        public string TitleRecipe { get=> titleRecipe; set => titleRecipe = value; }
        public PreparationStep PreparationStepAdd { get => preparationStep; set => preparationStep = value; }
        public static readonly BindableProperty PreparationStepOrderProperty =
                               BindableProperty.Create(nameof(PreparationStepOrder), typeof(int), typeof(Entry), 1);

        public int PreparationStepOrder
        {
            get => (int)GetValue(PreparationStepOrderProperty);
            set => SetValue(PreparationStepOrderProperty, value);
        }
        public AddRecipe()
        {
            InitializeComponent();
            BindingContext = this;

            IngredientList = new List<Ingredient>();
            PreparationStepList = new List<PreparationStep>();
        }
        private async void PickPhoto(object sender, EventArgs e)
        {
            ImageSource = await MediaPicker.Default.PickPhotoAsync();         
        }

        private async void AddRecipeValidation(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TitleRecipe))
            {
                await DisplayAlert("Erreur", "Entrez un nom de recette.", "Ok");
                return;
            }

            if (ImageSource != null)
            {
                // save the file into local storage
                ImageSourcePath = Path.Combine(FileSystem.Current.AppDataDirectory, $"{TitleRecipe.Replace(" ", "")}.{ImageSource.FileName}");

                using Stream sourceStream = await ImageSource.OpenReadAsync();
                using FileStream localFileStream = File.OpenWrite(ImageSourcePath);

                await sourceStream.CopyToAsync(localFileStream);
            }

            RecipeType newRecipeType = GetSelectedRecipeType();
            Priority selectedPriority = GetSelectedPriority();
            string authorMail = CurrentUser.Mail;

            Recipe newRecipe = new Recipe
                (
                    TitleRecipe,
                    newRecipeType,
                    selectedPriority,
                    null,
                    authorMail,
                    ImageSourcePath
                );
            newRecipe.PreparationSteps.AddRange(PreparationStepList);
            newRecipe.Ingredients.AddRange(IngredientList);

            bool isRecipeSave = Master.Recipe.AddRecipeToData(newRecipe);

            // Save data.
            Debug.Write($"[ {DateTime.Now:H:mm:ss} ] Saving...\t");
            Master.Data.SaveData();

            Debug.WriteLine("Done.");
            Debug.WriteLine(FileSystem.Current.AppDataDirectory);

            if (isRecipeSave)
            {
                await DisplayAlert("Succ�s", "La recette a �t� ajout�e avec succ�s", "OK");
            }
            else
            {
                await DisplayAlert("Echec", "La recette n'a pas �t� ajout�e", "OK");
            }

            newRecipe = new Recipe("Nouvelle Recette");

            PreparationStepList.Clear();
            IngredientList.Clear();
            await Navigation.PopModalAsync();
        }

        private void AddStepRecipe(object sender, EventArgs e)
        {
            string description = PreparationDescription.Text;

            PreparationStep PreparationStepAdd = new PreparationStep
                (
                    PreparationStepOrder,
                    description
                );
            PreparationStepList.Add( PreparationStepAdd );

            PreparationStepOrder++;
            PreparationDescription.Text = string.Empty;
        }

        private void RemoveStepRecipe(object sender, EventArgs e)
        {
            if (PreparationStepList.Count > 0)
            {
                PreparationStepList.RemoveAt(PreparationStepList.Count - 1);
                PreparationStepOrder--;
                DisplayAlert("Suppression", "La suppression de l'�tape de la recette est effectu�e avec succ�s", "Ok");
            }
            else
            {
                DisplayAlert("Liste d'�tape vide", "Suppression impossible car la liste des �tapes de description est vide.", "Ok");
            }
        }

        private void AddIngredient(object sender, EventArgs e)
        {
            if (nameIngredient.Text is null || quantityNumber.Text is null)
            {
                DisplayAlert("Warning", "some values are null, please provide correct values.", "Ok");
                return;
            }

            string ingredientName = nameIngredient.Text;
            int numberQuantity = Convert.ToInt32(quantityNumber.Text);
            Unit unitQuantity = (Unit)UnitPicker.SelectedItem;

            Ingredient newIngredient = new Ingredient
            (
                ingredientName,
               new Quantity
                (
                    numberQuantity,
                    unitQuantity
                )
            );
            IngredientList.Add( newIngredient );

            nameIngredient.Text = string.Empty;
            quantityNumber.Text = string.Empty;
            UnitPicker.SelectedItem = null;
        }

        private void RemoveIngredient(object sender, EventArgs e)
        {
            if (IngredientList.Count > 0)
            {
                IngredientList.RemoveAt(IngredientList.Count - 1);
                DisplayAlert("Suppression", "La suppression de l'ingr�dient est effectu�e avec succ�s", "Ok");
            }
            else
            {
                DisplayAlert("Liste d'ingr�dient vide", "Suppression impossible car la liste des ingr�dients est vide.", "Ok");
            }
        }

        private async void Import_Clicked(object sender, EventArgs e)
        {
            try
            {
                var result = await FilePicker.Default.PickAsync();
                if (result != null)
                {
                    Master.Data.Import<Recipe>(result.FullPath);

                    // Save data.
                    Debug.Write($"[ {DateTime.Now:H:mm:ss} ] Saving...\t");
                    Master.Data.SaveData();

                    Debug.WriteLine("Done.");
                    Debug.WriteLine(FileSystem.Current.AppDataDirectory);

                    await DisplayAlert("Import", "Recipe was successfully imported.", "Ok");
                    await Navigation.PopModalAsync();
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Import", $"Unable to import file.\n{ex.Message}", "Ok");
            }
        }

        private RecipeType GetSelectedRecipeType()
        {
            if (CheckEntree.IsChecked)
            {
                 return RecipeType.Starter;
            }
            else if (CheckPlat.IsChecked)
            {
                return RecipeType.Dish;
            }
            else if (CheckDessert.IsChecked)
            {
                return RecipeType.Dessert;
            }
            else
            {
                return RecipeType.Unspecified;
            }
        }
        private Priority GetSelectedPriority()
        {
            if (checkEconomique.IsChecked)
            {
                return Priority.Economic;
            }
            else if (checkFast.IsChecked)
            {
                return Priority.Fast;
            }
            else if (checkEasy.IsChecked)
            {
                return Priority.Easy;
            }
            else if (checkLight.IsChecked)
            {
                return Priority.Light;
            }
            else if (checkGourmet.IsChecked)
            {
                return Priority.Gourmet;
            }
            else
            {
                return Priority.Gourmet;
            }
        }


    }
}