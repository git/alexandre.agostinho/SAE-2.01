using AppException;
using Model;
using System.Diagnostics;

namespace Views;

public partial class Login : ContentPage
{
	MasterManager Master => (Application.Current as App).Master;

	public Login()
	{
		InitializeComponent();
		BindingContext = Master.User;
	}

    private void LoginButton_Clicked(object sender, EventArgs e)
    {
		string mail = login_entry.Text;
		string password = passwd_entry.Text;
        var usermgr = Master.User;

        if (firstLogin_checkbox.IsChecked)
		{
            try
            {
                usermgr.AddUserToData(usermgr.CreateUser(mail, password));

                // Save data.
                Debug.Write($"[ {DateTime.Now:H:mm:ss} ] Saving...\t");
                Master.Data.SaveData();

                Debug.WriteLine("Done.");
                Debug.WriteLine(FileSystem.Current.AppDataDirectory);
            }
            catch (BadMailFormatException)
            {
                DisplayAlert("Bad mail format", "The mail you entered do not respect the format of a mail address.", "Ok");
                return;
            }
        }

        if (!usermgr.LogIn(mail, password))
		{
            DisplayAlert("Wrong informations", "Incorrect mail or password have been entered.", "Ok");
        }
		else
		{
			Navigation.PopModalAsync();
		}
    }
}
