using DataPersistence;
using Model;
using System.Diagnostics;

namespace Views;

public partial class ContainerFlyout : ContentView
{
    public MasterManager Master => (Application.Current as App).Master;

    public ContainerFlyout()
	{
        InitializeComponent();
        BindingContext = Master.User;
    }

    #region Bindable XAML Properties
    // Bind MyFlyoutContent
    public static readonly BindableProperty MyFlyoutContentProperty =
        BindableProperty.Create("MyFlyoutContent", typeof(View), typeof(ContainerFlyout), new Grid());

    public View MyFlyoutContent
    {
        get => (View)GetValue(MyFlyoutContentProperty);
        set => SetValue(MyFlyoutContentProperty, value);
    }

    // bind NeedReturn
    public static readonly BindableProperty NeedReturnProperty =
        BindableProperty.Create("NeedReturn", typeof(bool), typeof(Border), false);

    public bool NeedReturn
    {
        get => (bool)GetValue(NeedReturnProperty);
        set => SetValue(NeedReturnProperty, value);
    }
    #endregion

    public async void ProfileButton_Clicked(object sender, EventArgs e)
    {
        await Navigation.PushModalAsync(new MyProfil());
    }

    public async void ConnectionButton_Clicked(object sender, EventArgs e)
    {
        await Navigation.PushModalAsync(new Login());
    }

    public void Logout_Clicked(object sender, EventArgs e)
    {
        Master.User.LogOut();
        Navigation.PopModalAsync();
    }

    public async void ChangeProfilePict_Clicked(object sender, EventArgs e)
    {
        FileResult photo = await MediaPicker.Default.PickPhotoAsync();

        if (photo != null)
        {
            // save the file into local storage
            string localFilePath = Path.Combine(FileSystem.Current.AppDataDirectory, $"{Master.User.CurrentConnected.Mail}.{photo.FileName}");

            using Stream sourceStream = await photo.OpenReadAsync();
            using FileStream localFileStream = File.OpenWrite(localFilePath);

            await sourceStream.CopyToAsync(localFileStream);

            Master.User.CurrentConnected.ProfilePict = localFilePath;

            // Save data.
            Debug.Write($"[ {DateTime.Now:H:mm:ss} ] Saving...\t");
            Master.Data.SaveData();

            Debug.WriteLine("Done.");
            Debug.WriteLine(FileSystem.Current.AppDataDirectory);
        }
    }
}
