using Model;

namespace Views;

public partial class MiniHeader : ContentView
{
	MasterManager Master = (Application.Current as App).Master;
    public Recipe Recipe => Master.Recipe.CurrentSelected;


    public MiniHeader()
	{
		InitializeComponent();
		BindingContext = Master.User.CurrentConnected;
	}

	public static readonly BindableProperty TitleMiniProperty =
		BindableProperty.Create(nameof(TitleMini), typeof(string), typeof(Label), "Erreur de titre");

	public string TitleMini
    {
		get => (string)GetValue(TitleMiniProperty); 
		set => SetValue(TitleMiniProperty, value);
	}
    // bind NeedReturn
    public static readonly BindableProperty NeedReturnProperty =
        BindableProperty.Create("NeedReturn", typeof(bool), typeof(Border), false);

    public bool NeedReturn
    {
        get => (bool)GetValue(NeedReturnProperty);
        set => SetValue(NeedReturnProperty, value);
    }
}