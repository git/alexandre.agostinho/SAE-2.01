using Model;
using CommunityToolkit.Maui.Storage;

namespace Views;

public partial class RecipeCase : ContentView
{
    public MasterManager Master => (Application.Current as App).Master;
    public Recipe Recipe => Master.Recipe.CurrentSelected;

    public RecipeCase()
	{
		InitializeComponent();
	}

    public static readonly BindableProperty CaseImageSourceProperty =
        BindableProperty.Create("CaseImageSource", typeof(ImageSource), typeof(Image));

    public ImageSource CaseImageSource
    {
        get => (ImageSource)GetValue(CaseImageSourceProperty);
        set => SetValue(CaseImageSourceProperty, value);
    }

    public static readonly BindableProperty RecipeTitleProperty =
        BindableProperty.Create("RecipeTitle", typeof(string), typeof(Label));

    public string RecipeTitle
    {
        get => (string)GetValue(RecipeTitleProperty);
        set => SetValue(RecipeTitleProperty, value);
    }
    private async void ImageButton_Clicked(object sender, EventArgs e)
    {
        Master.Recipe.CurrentSelected = (Recipe)(sender as ImageButton).BindingContext;
        await Navigation.PushModalAsync(new ViewRecette());
    }
}
