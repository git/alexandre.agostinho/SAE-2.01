namespace Views;

public partial class CustomHeader : ContentView
{
	public CustomHeader()
	{
		InitializeComponent();
    }

    private void ImageButton_Clicked(object sender, EventArgs e)
    {
		(App.Current.MainPage as Shell).FlyoutIsPresented ^= true;
    }
}