namespace Views;

public partial class ReturnButton : ContentView
{
	public ReturnButton()
	{
		InitializeComponent();
	}

    // bind NeedReturn
    public static readonly BindableProperty NeedReturnProperty =
		BindableProperty.Create("NeedReturn", typeof(bool), typeof(Border), false);

	public bool NeedReturn
	{
		get => (bool)GetValue(NeedReturnProperty);
		set => SetValue(NeedReturnProperty, value);
	}

	public void OnClickedReturn(object sender, EventArgs e)
	{
		Navigation.PopModalAsync();
	}
}
