using Model;

namespace Views;

public partial class ContainerBase : ContentView
{
    public MasterManager Master => (Application.Current as App).Master;

    public bool NeedReturn
    {
        get => container_flayout.NeedReturn;
        set => container_flayout.NeedReturn = value;
    }

	public ContainerBase()
	{
		InitializeComponent();
	}


    // Bind MyContent
    public static readonly BindableProperty MyContentProperty =
		BindableProperty.Create("MyContent", typeof(View), typeof(ContainerBase), new Grid());

	public View MyContent
	{
		get => (View)GetValue(MyContentProperty);
		set => SetValue(MyContentProperty, value);
	}

    // Bind MyFlyoutContent
    public static readonly BindableProperty MyFlyoutContentProperty =
        BindableProperty.Create("MyFlyoutContent", typeof(View), typeof(ContainerBase), new Grid());

    public View MyFlyoutContent
    {
        get => (View)GetValue(MyFlyoutContentProperty);
        set => SetValue(MyFlyoutContentProperty, value);
    }
}
