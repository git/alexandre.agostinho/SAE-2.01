﻿using System;
using System.Globalization;

namespace Views
{
    public class EnumToValuesConverter<T> : IValueConverter where T : struct, System.Enum
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            IEnumerable<T> result = Enum.GetValues<T>();
            if (parameter != null && parameter is string)
            {
                var names = (parameter as string).Split(" ");
                var excludedValues = names.Select(n => Enum.TryParse(n, true, out T result) ? (true, result) : (false, default(T)))
                                        .Where(tuple => tuple.Item1)
                                        .Select(tuple => tuple.Item2)
                                        .Distinct();

                result = result.Except(excludedValues).ToArray<T>();
            }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
