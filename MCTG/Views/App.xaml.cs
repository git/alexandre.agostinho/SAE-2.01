﻿using Model;
using FakePersistance;
using DataPersistence;
using Managers;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization.Json;

namespace Views
{
    public partial class App : Application
    {
        /// <summary>
        /// Master manager - access to the Model.
        /// </summary>
        public MasterManager Master { get; private set; }

        public App()
        {
            Debug.WriteLine("Hello, World!\n\n");

            string path = FileSystem.Current.AppDataDirectory;           // - path to the save file
            string strategy = "json";    // - strategy is 'xml' or 'json' (/!\ this is case sensitive)

            // Initialize the data serializer
            IDataSerializer dataSerializer = (strategy == "xml") ?
                  new DataContractXML(path)
                : new DataContractJSON(path);

            // Initialize the data manager
            IDataManager dataManager;
            if (!File.Exists(Path.Combine(path, $"data.{strategy}")))
            {
                var data = LoadJSONBundledFilesAsync("data.json");
                dataManager = new DataDefaultManager(dataSerializer, data);
            }
            else
            {
                dataManager = new DataDefaultManager(dataSerializer);
                dataManager.LoadData();
            }

            // Initialize the other managers
            IRecipeManager recipeManager = new RecipeDefaultManager(dataManager);
            IPasswordManager passwordManager = new PasswordSHA256Manager();
            IUserManager userManager = new UserDefaultManager(dataManager, passwordManager);

            // Initialize the master manager
            Master = new MasterManager(dataManager, recipeManager, userManager);

            // Save data.
            Debug.Write($"[ {DateTime.Now:H:mm:ss} ] Saving...\t");
            Master.Data.SaveData();
            Debug.WriteLine("Done.");

            InitializeComponent();

            UserAppTheme = AppTheme.Light;
            MainPage = new Home();
            //MainPage = new MyPosts();
        }

        protected override void OnSleep()
        {
            // Save data.
            Debug.Write($"[ {DateTime.Now:H:mm:ss} ] Saving...\t");
            Master.Data.SaveData();

            Debug.WriteLine("Done.");
            Debug.WriteLine(FileSystem.Current.AppDataDirectory);

            base.OnSleep();
        }

        
        /// <summary>
        /// Load XML raw assets from data.
        /// </summary>
        /// <param name="path">The path in the raw assets directory.</param>
        /// <returns>A dictionary containing the data loaded.</returns>
        private static IDictionary<string, List<object>> LoadXMLBundledFilesAsync(string path)
        {
            DataContractSerializerSettings _dataContractSerializerSettings =
                new DataContractSerializerSettings()
                {
                    KnownTypes = new Type[]
                    {
                        typeof(Recipe), typeof(RecipeType), typeof(Priority), typeof(Review), typeof(User), typeof(Ingredient), typeof(Quantity)
                    },
                    PreserveObjectReferences = true
                };
            var serializer = new DataContractSerializer(typeof(Dictionary<string, List<object>>), _dataContractSerializerSettings);
            IDictionary<string, List<Object>> data;

            using Stream stream = FileSystem.Current.OpenAppPackageFileAsync(path).Result;
            data = serializer.ReadObject(stream) as IDictionary<string, List<Object>>;

            return data;
        }

        /// <summary>
        /// Load JSON raw assets from data.
        /// </summary>
        /// <param name="path">The path in the raw assets directory.</param>
        /// <returns>A dictionary containing the data loaded.</returns>
        private static IDictionary<string, List<object>> LoadJSONBundledFilesAsync(string path)
        {
            DataContractJsonSerializerSettings _dataContractJsonSerializerSettings =
                new DataContractJsonSerializerSettings()
                {
                    KnownTypes = new Type[]
                    {
                        typeof(Recipe), typeof(RecipeType), typeof(Priority), typeof(Review), typeof(User), typeof(Ingredient), typeof(Quantity)
                    }
                };
            var jsonSerializer = new DataContractJsonSerializer(typeof(Dictionary<string, List<object>>), _dataContractJsonSerializerSettings);
            IDictionary<string, List<Object>> data;

            using Stream stream = FileSystem.Current.OpenAppPackageFileAsync(path).Result;
            data = jsonSerializer.ReadObject(stream) as IDictionary<string, List<Object>>;

            return data;
        }
    }
}
