﻿using ConsoleApp.Menu.Core;
using Model;
using System.Text;

namespace ConsoleApp.Menu
{
    internal class ShowRecipeInfos : PlainText
    {
        public Recipe Recipe { get; private set; }

        public ShowRecipeInfos(Recipe recipe)
            : base("")
        {
            Recipe = recipe;
        }

        public override void Display()
        {
            StringBuilder sb = new StringBuilder($"[Recipe n°{Recipe.Id}] - {Recipe.Title}\n");
            foreach (PreparationStep ps in Recipe.PreparationSteps)
            {
                sb.AppendFormat("\t* {0}\n", ps.ToString());
            }
            sb.AppendLine();
            sb.AppendLine(Recipe.ConcatIngredients());
            sb.AppendLine();
            foreach (Review review in Recipe.Reviews)
            {
                sb.AppendLine(review.ToString());
            }
            sb.AppendLine();
            sb.AppendLine($"Posted by: {Recipe.AuthorMail?.ToString()}");

            Console.WriteLine(sb);
        }
    }
}
