﻿using Model;
using ConsoleApp.Menu.Core;

namespace ConsoleApp.Menu
{
    /// <summary>
    /// Main menu of the console. Contain the first interaction menus.
    /// </summary>
    internal class MainMenu : Menu<IMenu>
    {
        private MasterManager _masterMgr;

        public MainMenu(MasterManager masterManager)
            : base("Main menu")
        {
            _masterMgr = masterManager;

            _allSelectors.Add(
                new Selector<IMenu>(new SearcherRecipe(_masterMgr), "Recipe search"));

            _allSelectors.Add(
                new Selector<IMenu>(new ConnectionMenu(_masterMgr), "Connection"));
            _allSelectors.Add(
                new Selector<IMenu>(new ProfileMenu(_masterMgr), "User profile"));
            _allSelectors.Add(
                new Selector<IMenu>(new LogoutButton(_masterMgr), "Logout"));

            _allSelectors.Add(
                new Selector<IMenu>(new AddRecipeMenu(_masterMgr), "Add recipe"));
            _allSelectors.Add(
                new Selector<IMenu>(new AddUserMenu(_masterMgr), "Add user"));

            _allSelectors.Add(
                new Selector<IMenu>(new ImportRecipeMenu(_masterMgr), "Import recipe"));
            _allSelectors.Add(
                new Selector<IMenu>(new ExportRecipeMenu(_masterMgr), "Export recipe"));
        }

        protected override List<Selector<IMenu>> SearchInSelection()
        {
            List<Selector<IMenu>> selectors = base.SearchInSelection();

            if (_masterMgr.User.CurrentConnected == null)
                return selectors.Except(selectors.Where(s => s.Line == "User profile"))
                                .Except(selectors.Where(s => s.Line == "Logout"))
                                .Except(selectors.Where(s => s.Line == "Add recipe")).ToList();
            else
                return selectors.Except(selectors.Where(s => s.Line == "Connection")).ToList();
        }
    }
}
