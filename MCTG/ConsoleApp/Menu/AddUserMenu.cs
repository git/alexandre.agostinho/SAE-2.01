﻿using ConsoleApp.Menu.Core;
using Model;

namespace ConsoleApp.Menu
{
    internal class AddUserMenu : Entry
    {
        MasterManager masterMgr;

        public AddUserMenu(MasterManager masterManager)
            : base("Add Manager",
                  new Entry.EntryStep("Mail: ", typeof(string)),
                  new Entry.EntryStep("Name: ", typeof(string)),
                  new Entry.EntryStep("Surname: ", typeof(string)),
                  new Entry.EntryStep("Password: ", typeof(string), true))
        {
            masterMgr = masterManager;
        }

        public override IMenu? Return()
        {
            string mail = _selectList[0].Item.Input;
            string name = _selectList[1].Item.Input;
            string surname = _selectList[2].Item.Input;
            string passwd = _selectList[3].Item.Input;

            User user = masterMgr.User.CreateUser(mail, passwd, name, surname);

            masterMgr.User.AddUserToData(user);
            return null;
        }
    }
}
