﻿using ConsoleApp.Menu.Core;
using Model;

namespace ConsoleApp.Menu
{
    internal class LogoutButton : PlainText
    {
        MasterManager _masterMgr;

        public LogoutButton(MasterManager masterManager)
            : base("Logout ? ('ENTER' yes, '<' no)")
        {
            _masterMgr = masterManager;
        }

        public override IMenu? Return()
        {
            _masterMgr.User.LogOut();
            return base.Return();
        }
    }
}
