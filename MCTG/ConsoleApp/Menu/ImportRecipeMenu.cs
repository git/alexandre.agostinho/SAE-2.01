﻿using ConsoleApp.Menu.Core;
using Model;

namespace ConsoleApp.Menu
{
    internal class ImportRecipeMenu : Entry
    {
        MasterManager masterMgr;

        public ImportRecipeMenu(MasterManager masterManager)
            : base("Import recipe",
                  new Entry.EntryStep("Path file: ", typeof(string)))
        {
            masterMgr = masterManager;
        }

        public override IMenu? Return()
        {
            string path = _selectList[0].Item.Input;
            try
            {
                masterMgr.Data.Import<Recipe>(path);
            }
            catch(ArgumentNullException e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("error: " + e.Message);
                Console.ResetColor();
                return this;
            }
            return null;
        }
    }
}
