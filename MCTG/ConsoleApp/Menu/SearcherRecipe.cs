﻿using ConsoleApp.Menu.Core;
using Model;

namespace ConsoleApp.Menu
{
    /// <summary>
    /// An utility to find a recipe.
    /// </summary>
    internal class SearcherRecipe : Menu<Recipe>
    {
        protected MasterManager _masterMgr;
        protected RecipeCollection _recipeCollectionOnSearch = new RecipeCollection("search");

        public SearcherRecipe(MasterManager masterManager) : base("Search recipe")
        {
            _masterMgr = masterManager;
        }

        #region Methods
        protected List<Selector<Recipe>> ConvertRecipeCollectionInSelectors()
        {
            List<Selector<Recipe>> newSelectors = new List<Selector<Recipe>>();
            foreach (Recipe recipe in _recipeCollectionOnSearch)
            {
                newSelectors.Add(new Selector<Recipe>(recipe, $"[{recipe.Id}]: {recipe.Title}"));
            }
            return newSelectors;
        }

        public override void Update()
        {
            _recipeCollectionOnSearch = _masterMgr.Recipe.GetAllRecipes();
            _allSelectors = ConvertRecipeCollectionInSelectors();
            base.Update();
        }

        public override IMenu? Return()
        {
            if (CurrentSelected == null)
                return this;

            return new ShowRecipeInfos(CurrentSelected);
        }
        #endregion
    }
}
