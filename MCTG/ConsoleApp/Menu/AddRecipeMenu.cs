﻿using ConsoleApp.Menu.Core;
using Model;

namespace ConsoleApp.Menu
{
    internal class AddRecipeMenu : Entry
    {
        MasterManager masterMgr;

        public AddRecipeMenu(MasterManager masterManager)
            : base("Add recipe",
                  new Entry.EntryStep("Title: ", typeof(string)),
                  new Entry.EntryStep("new step: ", typeof(string)),
                  new Entry.EntryStep("new step: ", typeof(string)),
                  new Entry.EntryStep("new step: ", typeof(string)),
                  new Entry.EntryStep("new step: ", typeof(string)))
        {
            masterMgr = masterManager;
        }

        public override IMenu? Return()
        {
            string title = _selectList[0].Item.Input;
            int order = 1;
            List<PreparationStep> steps = new List<PreparationStep>();
            for (int i = 1; i <= 4; i++)
            {
                if (string.IsNullOrEmpty(_selectList[i].Item.Input))
                    continue;

                steps.Add(new PreparationStep(order++, _selectList[i].Item.Input));
            }

            Recipe recipe = new Recipe(
                title: title,
                type: RecipeType.Unspecified,
                priority: Priority.Fast,
                id: null,
                authorMail: masterMgr.User.CurrentConnected?.Mail,
                picture: null)
            {
                PreparationSteps = steps
            };

            masterMgr.Recipe.AddRecipeToData(recipe);
            return null;
        }
    }
}
