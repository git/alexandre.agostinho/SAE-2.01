﻿using ConsoleApp.Menu.Core;
using Model;

namespace ConsoleApp.Menu
{
    internal class ExportRecipeMenu : SearcherRecipe
    {
        public ExportRecipeMenu(MasterManager masterManager)
            : base(masterManager)
        {
        }

        public override IMenu? Return()
        {
            if (CurrentSelected is null)
                throw new ArgumentNullException();

            Recipe recipe = CurrentSelected;
            string path = $"export_{string.Concat(CurrentSelected.Title.Where(c => !char.IsWhiteSpace(c)))}";

            // -- trying to put the right extention by checking the type of the DataManager...
            //if (object.ReferenceEquals(_masterMgr.DataMgr.GetType(), typeof(DataContractXML)))
            //    path += ".xml";
            //else if (object.ReferenceEquals(_masterMgr.DataMgr.GetType(), typeof(DataContractJSON)))
            //    path += ".json";

            path += ".xml";

            try
            {
                
                _masterMgr.Data.Export(recipe, path);
            }
            catch (ArgumentNullException e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("error: " + e.Message);
                Console.ResetColor();
                return this;
            }
            return null;
        }
    }
}
