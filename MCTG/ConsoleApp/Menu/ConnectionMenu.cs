﻿using ConsoleApp.Menu.Core;
using Model;

namespace ConsoleApp.Menu
{
    internal class ConnectionMenu : Entry
    {
        private MasterManager _masterMgr;
        private bool _wrongInput = false;

        public ConnectionMenu(MasterManager masterManager)
            : base("Connection",
                new Entry.EntryStep("Mail: ", typeof(string)),
                new Entry.EntryStep("Password: ", typeof(string), true))
        {
            _masterMgr = masterManager;
        }

        public override void Display()
        {
            base.Display();

            if (_wrongInput)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Wrong input...");
                Console.ResetColor();
            }
        }

        public override IMenu? Return()
        {
            string mail = _selectList[0].Item.Input;
            string password = _selectList[1].Item.Input;

            if (!_masterMgr.User.LogIn(mail, password))
            {
                _wrongInput = true;
                return this;
            }
            else
                return null;
        }
    }
}
