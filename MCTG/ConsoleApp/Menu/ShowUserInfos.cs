﻿using ConsoleApp.Menu.Core;
using Model;

namespace ConsoleApp.Menu
{
    internal class ShowUserInfos : PlainText
    {
        private MasterManager _masterMgr;

        public ShowUserInfos(MasterManager masterManager)
            : base("")
        {
            _masterMgr = masterManager;
        }

        public override void Display()
        {
            Console.WriteLine(
                   $"\nUser: {_masterMgr.User.CurrentConnected}\n\n"
                 + $"\tMail: {_masterMgr.User.CurrentConnected?.Mail}\n"
                 + $"\tName: {_masterMgr.User.CurrentConnected?.Name}\n"
                 + $"\tSurname: {_masterMgr.User.CurrentConnected?.Surname}\n");
        }
    }
}
