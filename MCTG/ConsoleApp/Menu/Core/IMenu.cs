﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp.Menu.Core
{
    /// <summary>
    /// Define a console menu with element selection.
    /// </summary>
    internal interface IMenu
    {
        /// <summary>
        /// True when enable, False otherwise.
        /// </summary>
        bool WriteMode { get; set; }

        /// <summary>
        /// A string input. Used for search string or entry input.
        /// </summary>
        StringBuilder InputStr { get; set; }

        /// <summary>
        /// Refresh the console with the updated display. This function dose not call Update().
        /// </summary>
        void Display();

        /// <summary>
        /// Update the parameters of the menu. Generally called before Display() to refresh the informations.
        /// </summary>
        void Update();

        /// <summary>
        /// Select the next element in the selection list.
        /// </summary>
        void SelectNext();

        /// <summary>
        /// Select the previous element in the selection list.
        /// </summary>
        void SelectPrevious();

        /// <summary>
        /// Enable the write mode.
        /// </summary>
        void EnableWriteMode();

        /// <summary>
        /// Disable the write mode.
        /// </summary>
        void DisableWriteMode();

        /// <summary>
        /// Toogle the write mode.
        /// </summary>
        void ToggleWriteMode();

        /// <summary>
        /// Define the comportement of the write mode. For instence: in the standard menu, it is used for the research of a line.
        /// </summary>
        /// <param name="cki">The key to deal with.</param>
        void WriteMenuMode(ConsoleKeyInfo cki);

        /// <summary>
        /// Execute some actions and then return.
        /// </summary>
        /// <returns>'null' when there is no menu after this selection. Otherwise the next menu.</returns>
        IMenu? Return();
    }
}
