﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp.Menu.Core
{
    internal abstract partial class Entry
    {
        /// <summary>
        /// Define a step of the Entry menu, or in other word, an entry itself.
        /// </summary>
        public class EntryStep
        {
            #region Attributes & Properties
            private readonly Type _entryType;

            /// <summary>
            /// The entry description. This text is generally placed before the input field.
            /// </summary>
            public string Description { get; private set; }

            /// <summary>
            /// Contain the input gave by the menu.
            /// </summary>
            public string Input { get; internal set; }

            /// <summary>
            /// Define whether the input need to be hidden. Useful for password.
            /// </summary>
            public bool Hidden { get; private set; }
            #endregion

            #region Constructors
            /// <summary>
            /// Constructor of the entry step.
            /// </summary>
            /// <param name="description">The text generally placed before the input in the menu.</param>
            /// <param name="type">The type of the returned input.</param>
            public EntryStep(string description, Type type, bool hidden = false)
            {
                Description = description;
                Input = "";
                _entryType = type;
                Hidden = hidden;
            }
            #endregion

            #region Methods
            /// <summary>
            /// Get the inputed string converted on this entry type.
            /// </summary>
            /// <returns>The converted string on the entry type.</returns>
            /// <exception cref="NotImplementedException">Throw when the entry type converter does not exist here.</exception>
            public object GetEntry()
            {
                try
                {
                    if (_entryType == typeof(string))
                        return Input;
                    if (_entryType == typeof(int))
                        return Int32.Parse(Input);
                    if (_entryType == typeof(DateTime))
                        return DateTime.Parse(Input);
                }
                catch (FormatException fe)
                {
                    Console.Error.WriteLine(fe);
                }

                throw new NotImplementedException("Error: parse of this type is not implemented.");
            }
            #endregion
        }
    }
}
