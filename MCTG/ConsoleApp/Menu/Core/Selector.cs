﻿using Model;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp.Menu.Core
{
    /// <summary>
    /// The selector of a menu.
    /// </summary>
    /// <typeparam name="T">The type of the selector.</typeparam>
    internal class Selector<T> : IEquatable<Selector<T>>
        where T : notnull
    {
        #region Attributes & Properties
        private string _line = "";

        /// <summary>
        /// The string that are displayed on the menu.
        /// </summary>
        public string Line
        {
            get => _line;
            private set
            {
                if (string.IsNullOrEmpty(value))
                    _line = "no data";
                else
                    _line = value;
            }
        }

        /// <summary>
        /// The item contained in the selector.
        /// </summary>
        public T Item { get; private set; }
        #endregion

        #region Constructors
        /// <summary>
        /// The constructor of the selector.
        /// </summary>
        /// <param name="item">The item to place inside.</param>
        /// <param name="line">The string to display in the menu.</param>
        public Selector(T item, string line = "")
        {
            Line = line;
            Item = item;
        }
        #endregion

        #region IEquatable implementation
        public virtual bool Equals(Selector<T>? other)
        {
            if (other == null) return false;
            if (other == this) return true;
            return other.Line.Equals(Line);
        }

        public override bool Equals(object? obj)
        {
            var item = obj as Selector<T>;
            if (item == null) return false;
            return Equals(obj);
        }

        public override int GetHashCode()
        {
            return Line.GetHashCode();
        }
        #endregion
    }
}
