﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp.Menu.Core
{
    /// <summary>
    /// Define a Plain text menu.
    /// <br/>This menu is a bit special. It display some text, and then, the only action that can be performed is the back return. Usefull for testing.
    /// </summary>
    internal class PlainText : IMenu
    {
        #region Constructors
        /// <summary>
        /// Constructor of the Plain text menu.
        /// </summary>
        /// <param name="text">The text buffer to display.</param>
        public PlainText(string text)
        {
            InputStr = new StringBuilder(text);
            WriteMode = false;
        }
        #endregion

        #region IMenu implementation
        public virtual IMenu? Return() { return null; }
        public virtual void Display()
        {
            Console.Clear();
            Console.WriteLine(InputStr);
        }

        public bool WriteMode { get; set; }
        public StringBuilder InputStr { get; set; }

        public void DisableWriteMode()
        {
            // Plain text does not need to do anything for this.
        }
        public void EnableWriteMode()
        {
            // Plain text does not need to do anything for this.
        }
        public void SelectNext()
        {
            // Plain text does not need to do anything for this.
        }
        public void SelectPrevious()
        {
            // Plain text does not need to do anything for this.
        }
        public void ToggleWriteMode()
        {
            // Plain text does not need to do anything for this.
        }
        public void Update()
        {
            // Plain text does not need to do anything for this.
        }
        public void WriteMenuMode(ConsoleKeyInfo cki)
        {
            // Plain text does not need to do anything for this.
        }
        #endregion
    }
}
