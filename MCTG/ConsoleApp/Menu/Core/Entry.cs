﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp.Menu.Core;

namespace ConsoleApp.Menu.Core
{
    /// <summary>
    /// Define an Entry menu.
    /// <br/>It allows you to navigate through the entries and completes them with a console input.
    /// </summary>
    internal abstract partial class Entry : Menu<Entry.EntryStep>
    {
        #region Attributes & Properties
        private List<EntryStep> _steps;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor of the entry menu, based on the Menu constructor.
        /// </summary>
        /// <param name="title">The title of this menu.</param>
        /// <param name="entrySteps">All the entries of this menu.</param>
        protected Entry(string title, params EntryStep[] entrySteps)
            : base(title)
        {
            _steps = entrySteps.ToList();
            _allSelectors = ConvertEntryStepsInSelector();
            _selectList = _allSelectors;
        }
        #endregion

        #region Methods
        private List<Selector<EntryStep>> ConvertEntryStepsInSelector()
        {
            List<Selector<EntryStep>> newSelectors = new List<Selector<EntryStep>>();
            foreach (EntryStep step in _steps)
            {
                newSelectors.Add(new Selector<EntryStep>(step, step.Description));
            }
            return newSelectors;
        }
        #endregion

        #region IMenu implementation
        public override void WriteMenuMode(ConsoleKeyInfo cki)
        {
            if (!WriteMode && cki.Key == ConsoleKey.R)
            {
                EnableWriteMode();
                if (CurrentSelected is null)
                    return;

                InputStr.Append(CurrentSelected.Input);
                CurrentSelected.Input = "";
                return;
            }

            if (WriteMode)
            {
                if (cki.Key == ConsoleKey.Escape)
                {
                    if (CurrentSelected is null)
                        throw new ArgumentNullException("CurrentSelected");

                    CurrentSelected.Input = InputStr.ToString();
                    DisableWriteMode();
                    InputStr.Clear();
                    return;
                }

                if (cki.Key == ConsoleKey.Backspace)
                {
                    if (InputStr.Length > 0) InputStr.Remove(InputStr.Length - 1, 1);
                    return;
                }

                InputStr.Append(cki.KeyChar);
            }
        }

        public override void Update()
        {
            if (_selectList.Count == 0)
            {
                CurrentSelected = default;
                return;
            }
            CurrentSelected = _selectList[CurrentLine].Item;
        }

        public override void Display()
        {
            StringBuilder displayItem = new StringBuilder();
            

            _screenDisplay.Clear();
            Console.Clear();

            _screenDisplay.AppendLine($"[ {Title} ]");
            _screenDisplay.AppendLine("-------------------------------------------");

            for (int i = 0; i < _selectList.Count; i++)
            {

                if (_selectList[i].Item.Hidden)
                    for (int _ = 0; _ < _selectList[i].Item.Input.Length; _++)
                        displayItem.Append('*');
                else
                    displayItem.Append(_selectList[i].Item.Input);


                if (CurrentLine == i)
                {
                    if (WriteMode)
                        _screenDisplay.Append($"W  ");
                    else
                        _screenDisplay.Append($">  ");
                }
                else
                    _screenDisplay.Append($"   ");

                _screenDisplay.Append($"{_selectList[i].Line} {displayItem}");



                if (CurrentLine == i && WriteMode)
                {
                    if (_selectList[i].Item.Hidden)
                        for (int _ = 0; _ < InputStr.Length; _++) _screenDisplay.Append('*');
                    else
                        _screenDisplay.Append(InputStr);
                }
                _screenDisplay.AppendLine();
                displayItem.Clear();
            }


            if (_selectList.Count == 0)
                _screenDisplay.AppendLine("Empty...");

            _screenDisplay.AppendLine(
                "\n\nHint:\n^:previous, v:next, <:back, -enter-:return, r:write, -escape-:exit search mode");
            Console.WriteLine(_screenDisplay);
        }
        #endregion
    }
}
