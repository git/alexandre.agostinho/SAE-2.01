﻿using Model;

namespace ConsoleApp.Menu
{
    internal class SearchUserRecipes : SearcherRecipe
    {
        public SearchUserRecipes(MasterManager masterManager) : base(masterManager)
        {
        }

        public override void Update()
        {
            if (_masterMgr.User.CurrentConnected is null)
                throw new ArgumentNullException();

            _recipeCollectionOnSearch = _masterMgr.Recipe.GetRecipeByAuthor(_masterMgr.User.CurrentConnected.Mail);
            _allSelectors = ConvertRecipeCollectionInSelectors();

            _selectList = SearchInSelection();

            if (_selectList.Count == 0)
            {
                CurrentSelected = default;
                return;
            }
            CurrentSelected = _selectList[CurrentLine].Item;
        }
    }
}
