﻿using ConsoleApp.Menu.Core;
using Model;

namespace ConsoleApp.Menu
{
    internal class ProfileMenu : Menu<IMenu>
    {
        public ProfileMenu(MasterManager masterManager)
            : base("Profile")
        {
            _allSelectors.Add(new Selector<IMenu>(
                new ShowUserInfos(masterManager),
                "My informations"));

            _allSelectors.Add(new Selector<IMenu>(
                new SearchUserRecipes(masterManager),
                "My recipes"));

        }
    }
}
