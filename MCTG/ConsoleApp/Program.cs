﻿using ConsoleApp;
using Model;
using DataPersistence;
using FakePersistance;
using Managers;


namespace ConsoleApp;

public static class Program
{
    public static void Main(string[] args)
    {
        Console.WriteLine("Hello, World!\n\n");

        string path = "";           // - path to the save file
        string strategy = "xml";    // - strategy is 'xml' or 'json' (/!\ this is case sensitive)

        // Initialize the data serializer
        IDataSerializer dataSerializer = (strategy == "xml") ?
              new DataContractXML(path)
            : new DataContractJSON(path);

        // Initialize the data manager
        IDataManager dataManager = (!File.Exists(Path.Combine(path, $"data.{strategy}"))) ?
              new DataDefaultManager(new Stubs())
            : new DataDefaultManager(dataSerializer);

        // Initialize the other managers
        IRecipeManager recipeManager = new RecipeDefaultManager(dataManager);
        IPasswordManager passwordManager = new PasswordSHA256Manager();
        IUserManager userManager = new UserDefaultManager(dataManager, passwordManager);

        // Initialize the master manager
        MasterManager Master = new MasterManager(dataManager, recipeManager, userManager);
        Master.Setup();

        MenuManager menuMgr = new MenuManager(Master);
        menuMgr.Loop();

        // Change the data serializer if the one in place is 'Stubs'
        if (Master.Data.Serializer.GetType() == typeof(Stubs))
        {
            var data = Master.Data.Data;
            dataManager = new DataDefaultManager(dataSerializer, data);
            Master = new MasterManager(dataManager, recipeManager, userManager);
        }

        // Save data.
        Console.Write("[ --SAVE-- ]:\t"); Master.Data.SaveData(); Console.WriteLine("Done.");
    }
}
