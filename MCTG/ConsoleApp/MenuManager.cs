﻿using ConsoleApp.Menu;
using Model;
using DataPersistence;
using ConsoleApp.Menu.Core;

namespace ConsoleApp
{
    /// <summary>
    /// Manage the menus of the console application.
    /// </summary>
    internal class MenuManager
    {
        #region Attributes & Properties
        /// <summary>
        /// The manager that contains usefull data taken from the model.
        /// </summary>
        public MasterManager MasterMgr { get; private set; }

        /// <summary>
        /// Each menu called are push in this stack. Then, to return back, we pop this stack to retrive the previous menu.
        /// </summary>
        public Stack<IMenu> MenuCallStack { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor of the MenuManager class. This constructor allows you to give the first menu of the call stack, wich is usefull for testing.
        /// </summary>
        /// <param name="masterManager">The data manager needed by the menus inside.</param>
        /// <param name="firstMenu">The starting menu, the first that will be push on the call stack.</param>
        public MenuManager(MasterManager masterManager, IMenu firstMenu)
        {
            MasterMgr = masterManager;
            MenuCallStack = new Stack<IMenu>();
            MenuCallStack.Push(firstMenu);
        }

        /// <summary>
        /// Constructor of the MenuManager class.
        /// </summary>
        /// <param name="masterManager">The data manager needed by the menus inside.</param>
        public MenuManager(MasterManager masterManager) : this(masterManager, new MainMenu(masterManager))
        { }
        #endregion

        #region Methods
        /// <summary>
        /// Main loop. Loop while the menu call stack is not empty.
        /// </summary>
        public void Loop()
        {
            ConsoleKeyInfo cki;
            IMenu menuOnHead;
            do
            {
                menuOnHead = MenuCallStack.Peek();
                menuOnHead.Update();
                menuOnHead.Display();

                cki = Console.ReadKey(true);
                switch (cki.Key)
                {
                    case ConsoleKey.DownArrow:
                        menuOnHead.SelectNext();
                        break;
                    case ConsoleKey.UpArrow:
                        menuOnHead.SelectPrevious();
                        break;
                    case ConsoleKey.Enter:
                        IMenu? retMenu = menuOnHead.Return();
                        if (retMenu is null) MenuCallStack.Pop();
                        else if (ReferenceEquals(retMenu, menuOnHead)) break;
                        else MenuCallStack.Push(retMenu);
                        break;
                    case ConsoleKey.LeftArrow:
                        MenuCallStack.Pop();
                        break;
                    default:
                        menuOnHead.WriteMenuMode(cki);
                        break;
                }
            } while (MenuCallStack.Count > 0);
        }
        #endregion
    }
}
