﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppException
{
    public class UserException : Exception
    {
        public UserException() : base("Somthing went wrong with an User.") { }
        public UserException(string message) : base(message) { }
    }
}
