﻿namespace AppException
{
    public class UserNotFoundException : UserException
    {
        public UserNotFoundException() : base("User not found.") { }
        public UserNotFoundException(string userMail) : base($"User with mail: '{userMail}' not found.") { }
    }
}
