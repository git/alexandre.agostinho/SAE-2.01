﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppException
{
    public class BadMailFormatException : UserException
    {
        public BadMailFormatException() : base("Invalid mail format.") { }
        public BadMailFormatException(string mail) : base($"'{mail}' is an invalid format.") { }
    }
}
