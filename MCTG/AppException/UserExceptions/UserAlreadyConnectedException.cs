﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppException
{
    public class UserAlreadyConnectedException : UserException
    {
        public UserAlreadyConnectedException() : base("An user is already connected.") { }
    }
}
