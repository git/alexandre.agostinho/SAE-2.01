﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppException
{
    public class NoUserConnectedException : UserException
    {
        public NoUserConnectedException() : base("No user is currently connected.") { }
    }
}
