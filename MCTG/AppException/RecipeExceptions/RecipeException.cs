﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace AppException
{
    public class RecipeException : Exception
    {
        public RecipeException() : base("Something went wrong with a recipe or a collection of recipe.") { }
        public RecipeException(string message) : base(message) { }
    }
}
