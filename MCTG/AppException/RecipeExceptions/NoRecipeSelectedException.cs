﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppException
{
    public class NoRecipeSelectedException : RecipeException
    {
        public NoRecipeSelectedException() : base("No recipe is currently selected to perform this action.") { }
    }
}
