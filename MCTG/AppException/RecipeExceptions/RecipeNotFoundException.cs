﻿
namespace AppException
{
    public class RecipeNotFoundException : RecipeException
    {
        public RecipeNotFoundException() : base("Recipe not found.") { }
        public RecipeNotFoundException(int id) : base($"Recipe id: '{id}'not found.") { }
    }
}
