﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Define how to manage an user.
    /// </summary>
    public interface IUserManager : INotifyPropertyChanged
    {
        /// <summary>
        /// Get the current connected user. Null if no user is connected.
        /// </summary>
        User? CurrentConnected { get; }

        /// <summary>
        /// Get the password manager used to hash passords.
        /// </summary>
        IPasswordManager PasswordManager { get; }

        /// <summary>
        /// Get a collection of all users in the data.
        /// </summary>
        /// <returns>A collection of all user.</returns>
        ICollection<User> GetAllUsers();

        /// <summary>
        /// Get an user by his email.
        /// </summary>
        /// <param name="mail">The user's mail.</param>
        /// <returns>The user corresponding to the mail.</returns>
        User GetUserFromMail(string mail);

        /// <summary>
        /// Create a new user. The mail and the password are required. Other can be null.
        /// <br/>This function use the password manager to hash the plain text password.
        /// </summary>
        /// <param name="mail">The user's mail address.</param>
        /// <param name="password">The user's plain text password.</param>
        /// <param name="name">The user's name.</param>
        /// <param name="surname">The user's surname.</param>
        /// <param name="profilePict">The user's profile picture.</param>
        /// <returns>A new user.</returns>
        User CreateUser(string mail, string password,
                        string? name = null, string? surname = null, string? profilePict = null);

        /// <summary>
        /// Add an user in the data.
        /// </summary>
        /// <param name="user">The user to add.</param>
        /// <returns>True is the user was correctly added to the data. False otherwise.</returns>
        bool AddUserToData(User user);

        /// <summary>
        /// Modify the currently connected user.
        /// </summary>
        /// <param name="newUser">An user containing new user's properties to changes.</param>
        /// <returns></returns>
        bool ModifyCurrentConnected(User newUser);

        /// <summary>
        /// Log in an user. If the connection succed, pass the connected user to <see cref="CurrentConnected"/>.
        /// </summary>
        /// <param name="mail">The User's mail.</param>
        /// <param name="password">The User's (plain text) password.</param>
        /// <returns>True if the connection succed, false otherwise.</returns>
        bool LogIn(string mail, string password);

        /// <summary>
        /// Log out the current connected user.
        /// </summary>
        void LogOut();
    }
}
