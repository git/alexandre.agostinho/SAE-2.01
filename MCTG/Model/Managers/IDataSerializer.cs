﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Interface that define the methods of a data serializer.
    /// </summary>
    public interface IDataSerializer
    {
        /// <summary>
        /// Save all the data in a file.
        /// </summary>
        /// <param name="elements">The data to save.</param>
        void Save(IDictionary<string, List<object>> elements);

        /// <summary>
        /// Load all the data from a file.
        /// </summary>
        /// <returns>The data loaded.</returns>
        IDictionary<string, List<object>> Load();

        /// <summary>
        /// Import an element to the collection of data.
        /// </summary>
        /// <typeparam name="T">The type of the element to impoert</typeparam>
        /// <param name="pathToImport">The path containing the name of the file.</param>
        /// <returns>A pair where the key is the entry in the data and the value is the value to add on this entry.</returns>
        public KeyValuePair<string, T> Import<T>(string pathToImport)
            where T : class;

        /// <summary>
        /// Export an element from the collection of data.
        /// </summary>
        /// <typeparam name="T">The type of the exported object.</typeparam>
        /// <param name="obj">The object to export.</param>
        /// <param name="pathToExport">The path containing the name of the file created.</param>
        public void Export<T>(T obj, string pathToExport)
            where T : class;
    }
}
