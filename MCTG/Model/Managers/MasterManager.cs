﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace Model
{
    /// <summary>
    /// The Main manager of the model.
    /// </summary>
    public class MasterManager
    {
        #region Attributes & Properties
        /// <summary>
        /// Manage the data of the application.
        /// </summary>
        public IDataManager Data { get; private set; }

        /// <summary>
        /// Manage the recipes of the application.
        /// </summary>
        public IRecipeManager Recipe { get; private set; }

        /// <summary>
        /// Manage the users of the application.
        /// </summary>
        public IUserManager User { get; private set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor of the MasterManager.
        /// </summary>
        /// <param name="dataManager">The data manager.</param>
        /// <param name="recipeManager">The recipes manager.</param>
        /// <param name="userManager">The users manager.</param>
        public MasterManager(IDataManager dataManager,
                             IRecipeManager recipeManager,
                             IUserManager userManager)
        {
            Data = dataManager;
            Recipe = recipeManager;
            User = userManager;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Setup all the necessary parameters before start.
        /// </summary>
        public void Setup()
        {
            Data.LoadData();
        }
        #endregion
    }
}
