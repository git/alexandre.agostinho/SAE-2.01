﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Define how to manage passwords.
    /// </summary>
    public interface IPasswordManager
    {
        /// <summary>
        /// Hash a plain text password.
        /// </summary>
        /// <param name="password">The plain password to hash.</param>
        /// <returns>The hashed password.</returns>
        public string HashPassword(string password);

        /// <summary>
        /// Verify a plain text password with a hashed one.
        /// </summary>
        /// <param name="hashedPassword">The hashed password.</param>
        /// <param name="password">The plain text password.</param>
        /// <returns>True is the password is correct, false otherwise.</returns>
        public bool VerifyPassword(string hashedPassword,string password);
    }
}
