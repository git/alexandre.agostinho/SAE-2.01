﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Define how to manage recipes.
    /// </summary>
    public interface IRecipeManager
    {
        /// <summary>
        /// Get or set the currently selected recipe.
        /// </summary>
        Recipe? CurrentSelected { get; set; }

        /// <summary>
        /// Get all the recipe in the data.
        /// </summary>
        /// <returns>The RecipeCollection containing the recipes stored in the data.</returns>
        RecipeCollection GetAllRecipes();

        /// <summary>
        /// Get the recipe corresponding to the id.
        /// </summary>
        /// <param name="id">The id of the recipe we want.</param>
        /// <returns>The recipe corresponding to the id.</returns>
        Recipe GetRecipeFromId(int id);

        /// <summary>
        /// Search in data the recipes containing this part of title.
        /// </summary>
        /// <param name="title">Search string.</param>
        /// <returns>The RecipeCollection of recipes corresponding to the search.</returns>
        RecipeCollection SearchRecipeByTitle(string title);

        /// <summary>
        /// Get all the recipes created by the author.
        /// </summary>
        /// <param name="authorMail">The author's mail.</param>
        /// <returns>The RecipeCollection of the recipe created by the author.</returns>
        RecipeCollection GetRecipeByAuthor(string authorMail);

        /// <summary>
        /// Get an ordored list of the recipes sorted by the priority list.
        /// </summary>
        /// <param name="priority">The priority list.</param>
        /// <returns>The RecipeCollection ordored by the priority list.</returns>
        RecipeCollection GetRecipesByPriorityOrder(IEnumerable<Priority> priority);

        /// <summary>
        /// Add a recipe to the data.
        /// </summary>
        /// <param name="recipe">The recipe to add.</param>
        /// <returns>Weither the adding succed or not.</returns>
        bool AddRecipeToData(Recipe recipe);

        /// <summary>
        /// Modify the <see cref="CurrentSelected"/> recipe with a new one in the data.
        /// </summary>
        /// <param name="newRecipe">The new recipe</param>
        /// <returns>Weither the modification succed or not.</returns>
        bool ModifyCurrentSelected(Recipe newRecipe);
    }
}
