﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Define how to manage data.
    /// </summary>
    public interface IDataManager
    {
        /// <summary>
        /// The data manager injected that know how to serialize the data.
        /// <br/><remarks><i>The setter is actually public for testing purpose. It will be private after.</i></remarks>
        /// <br/>See: <see cref="IDataSerializer"/>
        /// </summary>
        IDataSerializer Serializer { get; set; }

        /// <summary>
        /// The collection of all data. Each line of this dictionary has the type of the data as it key and the data for values.
        /// </summary>
        IDictionary<string, List<object>> Data { get; }


        /// <summary>
        /// Load the data. Used to update data written in the save file.
        /// <br/>See: <see cref="IDataSerializer.Load"/>
        /// </summary>
        void LoadData();

        /// <summary>
        /// Save the data. Call the Save method of the serializer.
        /// <br/>See: <see cref="IDataSerializer.Save(Dictionary{string, List{object}})"/>
        /// </summary>
        void SaveData();

        /// <summary>
        /// Import data from a file.
        /// <br/>See: <see cref="IDataSerializer.Import{T}(string)"/>
        /// </summary>
        /// <typeparam name="T">The type of data to import.</typeparam>
        /// <param name="pathOfTheFile">The path containing the name of the file created.</param>
        void Import<T>(string pathOfTheFile) where T : class;

        /// <summary>
        /// Export the data from the collection of data.
        /// <br/>See: <see cref="IDataSerializer.Export{T}(T, string)"/>
        /// </summary>
        /// <typeparam name="T">The type of data to export</typeparam>
        /// <param name="obj">The object to export</param>
        /// <param name="pathToExport">The path containing the name of the file created.</param>
        void Export<T>(T obj, string pathToExport) where T : class;

        /// <summary>
        /// Get a list of an item in the data.
        /// </summary>
        /// <typeparam name="T">The type of the item</typeparam>
        /// <returns>The list of all the item found in the data.</returns>
        ICollection<T> GetFromData<T>() where T : class;
    }
}
