﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Define a step of the preparation of a recipe.
    /// </summary>
    [DataContract(Name = "preparation-step")]
    public class PreparationStep : IEquatable<PreparationStep> , INotifyPropertyChanged
    {
        #region Attributes
        private string _description = "";
        private int _order = 1;
        public event PropertyChangedEventHandler? PropertyChanged;

        #endregion

        #region Properties
        /// <summary>
        /// The order of this step in the preparation of the meal.
        /// </summary>
        [DataMember(Name = "order")]
        public int Order 
        {
            get { return _order; }
            set
            {
                _order = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// The description of the task the user need to do for this step of the preparation. <br/>
        /// Set to "No description." when the value passed is null, empty or contain white spaces.
        /// </summary>
        [DataMember(Name = "description")]
        public string Description
        {
            get => _description;
            private set
            {
                if (string.IsNullOrWhiteSpace(value))
                    _description = "No description.";
                else
                    _description = value;
            }
        }

        protected void OnPropertyChanged([CallerMemberName] string? propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Construct a new step of preparation.
        /// </summary>
        /// <param _name="order">The number of the order in preparation</param>
        /// <param _name="description">The description of the task</param>
        public PreparationStep(int order, string description = "")
        {
            Order = order;
            Description = description;
        }
        #endregion

        #region Methods
        public virtual bool Equals(PreparationStep? other)
        {
            if (other == null) return false;
            if (other == this) return true;
            return Order.Equals(other.Order) && Description.Equals(other.Description);
        }

        public override bool Equals(object? obj)
        {
            var item = obj as PreparationStep;
            if (item == null) return false;
            return Equals(obj);
        }

        public override int GetHashCode()
        {
            return Order.GetHashCode() + Description.GetHashCode();
        }

        public override string ToString()
        {
            return $"{Order}- {Description}";
        }
        #endregion
    }
}
