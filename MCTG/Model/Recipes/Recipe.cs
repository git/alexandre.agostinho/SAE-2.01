using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;

namespace Model
{
    /// <summary>
    /// Define a Recipe for the preparation of a meal.
    /// </summary>
    [DataContract(Name = "recipe")]
    public class Recipe : IEquatable<Recipe>
    {
        #region Attributes
        [DataMember(Name = "title")]
        private string _title = "";

        [DataMember(Name = "image")]
        private string _image = "";
        
        [DataMember(Name = "authorMail")]
        private string _authorMail = "";
        #endregion

        #region Properties
        /// <summary>
        /// The ID of the recipe - allows you to compare and/or get this item in an easier way.
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; init; }

        /// <summary>
        /// List of reviews of this recipe.
        /// </summary>
        [DataMember(Name = "reviews")]
        public List<Review> Reviews { get; set; }

        /// <summary>
        /// AuthorMail's mail of the recipe.
        /// </summary>
        public string? AuthorMail
        {
            get => _authorMail;
            set
            {
                if (string.IsNullOrEmpty(value))
                    _authorMail = "admin@mctg.fr";
                else
                    _authorMail = value;
            }
        }

        /// <summary>
        /// Priority of this recipe.
        /// </summary>
        [DataMember(Name = "priority")]
        public Priority Priority { get;  set; }

        /// <summary>
        /// The Title of the recipe. <br/>
        /// Set to "No title." when the value passed is null, empty or contain white spaces.
        /// </summary>
        public string Title
        {
            get => _title;
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    _title = "No title.";
                else
                    _title = value;
            }
        }

        /// <summary>
        /// The image of the recipe. <br/>
        /// Set to "room_service_icon.png" when the value passed is null, empty or contain white space.
        /// </summary>
        public string? Image
        {
            get => _image;
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    _image = "room_service_icon.png";
                else
                    _image = value;
            }
        }

        /// <summary>
        /// The list of ingredients.
        /// </summary>
        [DataMember(Name = "ingredient")]
        public List<Ingredient> Ingredients { get; set; }


        /// <summary>
        /// The steps of the preparation. See: <see cref="PreparationStep"/>.
        /// </summary>
        [DataMember(Name = "preparation-steps")]
        public List<PreparationStep> PreparationSteps { get; set; }

        /// <summary>
        /// The type of recipe.
        /// </summary>
        [DataMember(Name = "type")]
        public RecipeType Type { get;  set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Construct a new recipe.
        /// </summary>
        /// <param name="title">The title of the recipe</param>
        /// <param name="type">The type of the recipe.</param>
        /// <param name="priority">The priority of this recipe.</param>
        /// <param name="id">The id of the recipe. If not given, get a new id.</param>
        /// <param name="authorMail">The name of the user that create this recipe.</param>
        /// <param name="picture"> The image that represent the recipe</param>
        public Recipe(string title, RecipeType type, Priority priority, int? id, string? authorMail, string? picture)
        {
            Title = title;
            Type = type;
            Priority = priority;
            Image = picture;
            AuthorMail = authorMail;

            PreparationSteps = new List<PreparationStep>();
            Ingredients = new List<Ingredient>();
            Reviews = new List<Review>();

            if (id == null)
            {
                var randomGenerator = RandomNumberGenerator.Create();
                byte[] data = new byte[16];
                randomGenerator.GetBytes(data);
                Id = Math.Abs(BitConverter.ToInt16(data));
            }
            else Id = (int)id;
        }

        /// <inheritdoc cref="Recipe.Recipe(string, RecipeType, Priority, int?, string?, string?)"/>
        public Recipe(string title, RecipeType type, Priority priority, int? id, string? authorMail)
            : this(title, type, priority, id, authorMail, null)
        {
        }

        /// <inheritdoc cref="Recipe.Recipe(string, RecipeType, Priority, int?, string?, string?)"/>
        public Recipe(string title, RecipeType type, Priority priority)
            : this(title, type, priority, null, null)
        {
        }

        /// <inheritdoc cref="Recipe.Recipe(string, RecipeType, Priority, int?, string?, string?)"/>
        public Recipe(string title)
            : this(title, RecipeType.Unspecified, Priority.Fast)
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// Add a review for the recipe.
        /// </summary>
        /// <param _name="review">The new review to add.</param>
        public void AddReview(Review review)
            => Reviews.Add(review);

        /// <summary>
        /// Get a string representing all the reviews and their informations.
        /// </summary>
        /// <returns></returns>
        public string GetReviews()
        {
            StringBuilder sb = new StringBuilder("Reviews:\n------------------------------\n");
            foreach (Review review in Reviews)
            {
                sb.AppendLine(review.ToString());
            }
            return sb.ToString();
        }

        /// <summary>
        /// Concatenate the list of ingredients in a single string
        /// </summary>
        /// <returns>The list of ingredients in string format</returns>
        public string ConcatIngredients()
        {
            StringBuilder sb = new StringBuilder();
            foreach (Ingredient ingredient in Ingredients)
            {
                sb.Append("\t- " + ingredient.ToString() + "\n");
            }

            return sb.ToString();
        }


        public virtual bool Equals(Recipe? other)
        {
            if (other == null) return false;
            if (other == this) return true;
            return Title.Equals(other.Title) && PreparationSteps.Equals(other.PreparationSteps);
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(obj, null)) return false;
            if (ReferenceEquals(obj, this)) return true;
            if (GetType() != obj.GetType()) return false;

            return Equals(obj as Recipe);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override string ToString()
        {
            return $"'{Title}' [{Id}] - {Type}, {Priority} | {AuthorMail} | {Image}";
        }
        #endregion
    }
}
