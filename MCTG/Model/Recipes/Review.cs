﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Model
{
    /// <summary>
    /// Define a Review of a recipe.
    /// </summary>
    [DataContract(Name = "review")]
    public class Review : IEquatable<Review>
    {
        #region Attributes & Properties
        [DataMember(Name = "stars")]
        private int _stars;

        [DataMember(Name = "content")]
        private string _content = "";

        /// <summary>
        /// The Id of the review.
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; init; }

        /// <summary>
        /// The mail of the author of the review.
        /// </summary>
        [DataMember(Name = "authorMail")]
        public string AuthorMail { get; private set; }

        /// <summary>
        /// The number of stars the review give.
        /// </summary>
        public int Stars
        {
            get => _stars;
            set
            {
                if (value < 0 || value > 5) throw new ArgumentException(nameof(Stars));
                else _stars = value;
            }
        }

        /// <summary>
        /// The comment in the review.
        /// </summary>
        public string Content
        {
            get => _content;
            set
            {
                if (string.IsNullOrEmpty(value)) _content = "No data...";
                else _content = value;
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor of a review.
        /// </summary>
        /// <param name="authorMail">The review's author's mail.</param>
        /// <param name="id">The id of the review.</param>
        /// <param name="stars">The mark to give for the recipe.</param>
        /// <param name="content">A comment about the recipe.</param>
        public Review(string authorMail, int? id, int stars, string content)
        {
            if (id == null)
            {
                var randomGenerator = RandomNumberGenerator.Create();
                byte[] data = new byte[16];
                randomGenerator.GetBytes(data);
                Id = Math.Abs(BitConverter.ToInt16(data));
            }
            else Id = (int)id;

            AuthorMail = authorMail;
            Stars = stars;
            Content = content;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public Review(string authorMail, int stars, string content)
            : this(authorMail, null, stars, content)
        {
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public Review(int stars, string content)
            : this("admin@mctg.fr", null, stars, content)
        {
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return $"{AuthorMail}: [ {Stars} stars ]\n{Content}";
        }

        public bool Equals(Review? other)
        {
            if (other is null) return false;
            return Id.Equals(other.Id);
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(obj, null)) return false;
            if (ReferenceEquals(obj, this)) return true;
            if (GetType() != obj.GetType()) return false;

            return Equals(obj);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
        #endregion
    }
}
