﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class ReadOnlyObservableRecipeCollection : ReadOnlyObservableCollection<Recipe>
    {
        private RecipeCollection _recipeObserved;

        public string Description => _recipeObserved.Description;

        public ReadOnlyObservableRecipeCollection(RecipeCollection recipeCollection)
            : base(recipeCollection)
        {
            _recipeObserved = recipeCollection;
        }
    }
}
