﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

namespace Model
{
    /// <summary>
    /// Define a collection of <see cref="Recipe"/>.
    /// <br/>This class is derived from <see cref="ObservableCollection{Recipe}"/>
    /// and implement <see cref="IEquatable{RecipeCollection}"/> and <see cref="ICloneable"/>.
    /// </summary>
    public class RecipeCollection : ObservableCollection<Recipe>, IEquatable<RecipeCollection>, ICloneable
    {
        #region Attributes
        private string _description = "";
        #endregion

        #region Properties
        /// <summary>
        /// A short description of what this collection contain. <br/>
        /// Set to "No description." when the value passed is null, empty or contain white spaces.
        /// </summary>
        public string Description
        {
            get => _description;
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    _description = "No description.";
                else
                    _description = value;

                OnPropertyChanged(new PropertyChangedEventArgs("Description"));
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Construct a new collection of recipes.
        /// </summary>
        /// <param name="description">A short description of what this list will contain.</param>
        /// <param name="recipes">Recipes to add in this new collection.</param>
        public RecipeCollection(string description, ICollection<Recipe> recipes)
            : base(recipes)
        {
            Description = description;
        }

        /// <inheritdoc cref="RecipeCollection.RecipeCollection(string, ICollection{Recipe})"/>
        public RecipeCollection(string description)
            : base()
        {
        }
        #endregion

        #region Methods
        public virtual bool Equals(RecipeCollection? other)
        {
            if (other == null) return false;
            if (other == this) return true;
            return this.Description.Equals(other.Description);
        }

        public override bool Equals(object? obj)
        {
            var item = obj as RecipeCollection;
            if (item == null) return false;
            return Equals(obj);
        }

        public override int GetHashCode()
        {
            return Description.GetHashCode();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder($"[RecipeCollection] - {Description}:\n");
            foreach (Recipe r in this)
            {
                sb.AppendFormat("\t - {0}\n", r.ToString());
            }
            return sb.ToString();
        }

        public object Clone()
        {
            return new RecipeCollection(
                description: this.Description,
                recipes: this.ToArray());
        }
        #endregion
    }
}
