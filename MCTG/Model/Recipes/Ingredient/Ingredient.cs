﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    [DataContract(Name = "ingredient")]
    public class Ingredient : INotifyPropertyChanged
    {
        #region Attributes
        [DataMember(Name = "id")]
        private string _name = "";

        [DataMember(Name = "quantity")]
        private Quantity _quantity = new Quantity(1, Unit.unit);

        public event PropertyChangedEventHandler? PropertyChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Property of the Ingredient's attribute _name. It raises an ArgumentNullException to prevent a nullable field.
        /// </summary>
        public string Name
        {
            get => _name;
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException("Impossible de ne pas avoir de nom pour l'ingrédient");
                }
                _name = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Property of the Ingredient's attribute Quantity. 
        /// </summary>
        public Quantity QuantityI
        {
            get { return _quantity; }
            set 
            { 
                _quantity = value; 
                OnPropertyChanged();
            }

        }


        public bool Equals(Ingredient? other)
        {
            if (other == null) { return false; }
            if (this == other) { return true; }
            return Name.Equals(other.Name);
        }
        #endregion

        #region Constructor

        public Ingredient(string name, Quantity quantity)
        {
            Name = name;
            QuantityI = quantity;
        }
        protected void OnPropertyChanged([CallerMemberName] string? propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        public override string ToString()
        {
            return $"{Name} ({QuantityI})";
        }
    }
}
