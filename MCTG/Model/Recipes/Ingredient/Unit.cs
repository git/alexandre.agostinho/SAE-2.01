﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Unit is an Enum that represents the units of quantities
    /// <list type="Unit, kG, mG, G, L, cL, mL"/>
    /// </summary>
    public enum Unit
    { unit, kG, mG, G, L, cL, mL };
}
