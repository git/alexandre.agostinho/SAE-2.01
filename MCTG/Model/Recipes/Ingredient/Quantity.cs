﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Quantity is a class which is associate to Ingedients. It represents the _quantity of every ingredient with an enum
    /// to indicate the unit of the _quantity.
    /// </summary>
    [DataContract(Name = "quantity")]
    public class Quantity
    {
        #region Attributes
        /// <summary>
        /// get the quatity of ingredient
        /// </summary>
        [DataMember(Name = "digit")]
        private int number;
        /// <summary>
        /// have the Unit enum of the _quantity of ingredient
        /// </summary>
        [DataMember(Name = "unit")]
        private Unit unit;

        #endregion

        #region Properties
        /// <summary>
        /// Represents the _quantity in digits. The null value raise an argumentException.
        /// </summary>
        public int Number
        {
            get { return number; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Si la quantité est inférieur à 0, enlever l'ingrédient!");
                }
                number = value;
            }
        }
        public Unit UnitQ
        {
            get => unit;
            set => unit = value;
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor of Quantity 
        /// </summary>
        /// <param _name="number"></param>
        /// <param _name="unit"></param>
        public Quantity(int number, Unit unit)
        {
            Number = number;
            UnitQ = unit;
        }
        #endregion

        public override string ToString()
        {
            return $"{Number} {UnitQ} de  : ";
        }
    }
}
