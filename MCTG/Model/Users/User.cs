
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace Model
{
    /// <summary>
    /// A user is an entity with a name, a surname, mail, profilePict and a list of priority.
    /// This user can login with an Id and a password
    /// </summary>
    [DataContract(Name = "user")]
    public class User : IEquatable<User> , INotifyPropertyChanged 
    {
        #region Private Attributes
        [DataMember(Name = "name")] private string _name = "";
        [DataMember(Name = "surname")] private string _surname = "";
        [DataMember(Name = "mail")] private string _mail = "";
        [DataMember(Name = "profilepic")] private string _profilePict = "default_picture.png";

        [DataMember(Name = "priorities")]
        private readonly List<Priority> _priorities = new List<Priority>
        {
            Priority.Gourmet,
            Priority.Economic,
            Priority.Fast,
            Priority.Light,
            Priority.Easy
        };



    public event PropertyChangedEventHandler? PropertyChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Property to get Name of users and a setter 
        /// </summary>
        /// <exception cref="ArgumentException" >Setter have Exception which is trigger when Name is null</exception>
        public string Name
        {
            get { return _name; }
            set
            {
             
                _name = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Property to get Surname of users and a setter 
        /// </summary>
        /// <exception cref="ArgumentException" >Setter have Exception which is trigger when Surname is null</exception>
        public string Surname
        {
            get { return _surname; }
            set
            {
             
                _surname = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Property to get mail of users and a setter 
        /// </summary>
        /// <exception cref="ArgumentException" >User's mail will serve to log the user. So there's no setter, just an init. User will enter one time his email at his
        /// account creation.</exception>
        public string Mail
        {
            get { return _mail; }
            private init
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException("Impossible d'avoir un champ Email vide!");
                }
                _mail = value;
            }
        }

        /// <summary>
        /// The user's hashed password. The hashed method is defined with the PasswordManager.
        /// <br/>See: <see cref="IPasswordManager"/>.
        /// </summary>
        [DataMember(Name = "hashedpass")]
        public string Password { get; private set; } = "";

        /// <summary>
        /// For now, we define the ProfilePict as a string which is "PhotoParDefaut"
        /// when the value is null.
        /// </summary>
        public string ProfilePict
        {
            get => _profilePict;
            set
            {
                _profilePict = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// This is the list of priorities specific tu the user. This list is initiate
        /// by default. User could change it at will.
        /// </summary>
        public List<Priority> Priorities => _priorities;
        #endregion

        #region Methods
        public override bool Equals(object? other)
        {
            if (ReferenceEquals(other, null)) return false;
            if (ReferenceEquals(other, this)) return true;
            if (GetType() != other.GetType()) return false;

            return Equals(other as User);
        }

        public bool Equals(User? other)
        {
            if (other == null) return false;
            if (other == this) return true;

            return Mail.Equals(other.Mail);
        }

        public override int GetHashCode()
        {
            throw new NotImplementedException();
        }

        protected void OnPropertyChanged([CallerMemberName] string? propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public override string ToString()
        {
            return $"{Name} {Surname}";
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Construtors of user.
        /// </summary>
        /// <param name="name">The name of the user</param>
        /// <param name="surname">The surname of the user</param>
        /// <param name="mail">The user needs an email to login.</param>
        /// <param name="hashedPassword">The password of the new user.</param>
        public User(string name, string surname, string mail, string hashedPassword)
        {
            Name = name;
            Surname = surname;
            Mail = mail;
            Password = hashedPassword;
            
        }

        /// <inheritdoc cref="User.User"/>
        /// <param name="profilePict">Profile picture of the new user.</param>
        public User(string name, string surname, string mail, string hashedPassword, string profilePict)
            : this(name, surname, mail, hashedPassword)
        {
            ProfilePict = profilePict;
        }

        /// <inheritdoc cref="User.User"/>
        public User()
            : this("John", "Doe", "truc@gmail.com", "mdp")
        {
        }

        /// <inheritdoc cref="User.User"/>
        /// <param name="user">The user to copy.</param>
        public User(User user)
        {
            Name = user.Name;
            Surname = user.Surname;
            Mail = user.Mail;
            Password = user.Password;
            _priorities = user._priorities;
            ProfilePict = user.ProfilePict;
        }
        #endregion
    }
}
