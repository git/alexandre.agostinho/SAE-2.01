﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// This is the list of priorities that user can define in his profil. Priorities
    /// are also present in recipes to match the first user's priority with the recipe's priority.
    /// </summary>
    public enum Priority
    { Economic, Fast, Easy, Light, Gourmet };

   
}


