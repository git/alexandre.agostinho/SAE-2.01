[![Build Status](https://codefirst.iut.uca.fr/api/badges/alexandre.agostinho/SAE-2.01/status.svg)](https://codefirst.iut.uca.fr/alexandre.agostinho/SAE-2.01)
[![Quality Gate Status](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=SAE-2.01_MCTG&metric=alert_status&token=55fb224b37553f8855c56cd9134e26bcab025bb4)](https://codefirst.iut.uca.fr/sonar/dashboard?id=SAE-2.01_MCTG)  
[![Code Smells](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=SAE-2.01_MCTG&metric=code_smells&token=55fb224b37553f8855c56cd9134e26bcab025bb4)](https://codefirst.iut.uca.fr/sonar/dashboard?id=SAE-2.01_MCTG)
[![Coverage](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=SAE-2.01_MCTG&metric=coverage&token=55fb224b37553f8855c56cd9134e26bcab025bb4)](https://codefirst.iut.uca.fr/sonar/dashboard?id=SAE-2.01_MCTG)
[![Maintainability Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=SAE-2.01_MCTG&metric=sqale_rating&token=55fb224b37553f8855c56cd9134e26bcab025bb4)](https://codefirst.iut.uca.fr/sonar/dashboard?id=SAE-2.01_MCTG)
[![Reliability Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=SAE-2.01_MCTG&metric=reliability_rating&token=55fb224b37553f8855c56cd9134e26bcab025bb4)](https://codefirst.iut.uca.fr/sonar/dashboard?id=SAE-2.01_MCTG)
[![Vulnerabilities](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=SAE-2.01_MCTG&metric=vulnerabilities&token=55fb224b37553f8855c56cd9134e26bcab025bb4)](https://codefirst.iut.uca.fr/sonar/dashboard?id=SAE-2.01_MCTG)  



# SAE 2.01 - Développement d'une application
### Ma Cuisine Trop Géniale ! - Application MAUI
<br>


## Documentation
La conception et la description de l'architecture du projet sont disponibles dans [le wiki](https://codefirst.iut.uca.fr/git/alexandre.agostinho/SAE-2.01/wiki/).

La documentation du code générée par Doxygen est disponible dans [CodeDoc](https://codefirst.iut.uca.fr/documentation/alexandre.agostinho/SAE-2.01/SAE-2.01_MCTG-documentation/doxygen/).

## Utilisation de l'application
L'application charge des données par défaut. Toute donnée supplémentaire est uniquement sauvegardée en local.  
Les données de l'application sont sauvegardées en format Json.

Il existe aussi quelques fonctionnalités moins évidantes à trouver :
- Vous pouvez changer de photo de profil en faisant un clique-droit dessus. Le clique-gauche permet de voir le profil.
- Vous pouvez exporter de recettes au format Json en faisant clique-droit sur une recette.

## Tests fonctionnels
Navigation dans les menus de l'application console :
- ` ^ ` *(fleche haute)* : sélection précédante,
- ` v ` *(fleche basse)* : sélection suivante,
- ` < ` *(fleche gauche)* : retour arrière,
- ` <enter> ` *(touche entrée)* : entrer dans la sélection / valider,
- ` r ` *(touche 'r')* : **activer le mode écriture**: rechercher dans les sélections / mettre du texte dans les zones de textes,
- ` <echap> ` *(touche echap)* : **désactiver le mode écriture** / **enregistrer le texte dans la zone de texte**.

⚠️ La navigation dans les menus de l'application console est assez spéciale. Elle est plutot complexe et surtout très mal optimisée (comme ce n'est que pour tester). Par exemple, pour entrer du texte dans un champ de texte : il faut activer le **mode écriture** avec la touche `r`, entrer son texte, **PUIS désactiver le mode écriture** avec la touche `<echap>`. Cette dernière étape permet d'enregistrer le texte dans le champ.

## Auteurs de l'application
- [Roxane Rossetto](https://codefirst.iut.uca.fr/git/roxane.rossetto)
- [Alexandre Agostinho](https://codefirst.iut.uca.fr/git/alexandre.agostinho)
